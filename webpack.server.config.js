/* This config file is only for transpiling the Express server file.
 * You need webpack-node-externals to transpile an express file
 * but if you use it on your regular React bundle, then all the
 * node modules your app needs to function get stripped out.
 *
 * Note: that prod and dev mode are set in npm scripts.
 */
const nodeExternals = require("webpack-node-externals");
const path = require("path");

process.env.NODE_PATH = "src";

module.exports = (env, argv) => {
  process.env.BABEL_ENV = argv.mode;
  process.env.NODE_ENV = argv.mode;

  const SERVER_PATH = "./server.js";

  console.log("- SERVER_PATH", SERVER_PATH, argv.mode);

  return {
    entry: {
      server: [SERVER_PATH]
    },
    output: {
      path: path.resolve(__dirname, "./dist/"),
      publicPath: "/",
      filename: "[name].js"
    },
    mode: argv.mode,
    target: "node",
    node: {
      // Need this when working with express, otherwise the build fails
      __dirname: false, // if you don't put this is, __dirname
      __filename: false // and __filename return blank or /
    },
    externals: [nodeExternals()], // Need this to avoid error when working with Express
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          include: path.resolve(__dirname, "./src/"),
          loader: "babel-loader",
          exclude: /node_modules/,
          query: {
            presets: ["react", "es2015"]
          }
        }
      ]
    },
    resolve: {
      modules: [path.resolve("./src"), "node_modules"]
    }
  };
};
