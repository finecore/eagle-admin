const bodyParser = require("body-parser");
const createError = require("http-errors");
const path = require("path");
const cors = require("cors");
const chalk = require("chalk");

// .env config.
require("dotenv").config();

const history = require("connect-history-api-fallback");
const express = require("express");
const app = express();

const staticFileMiddleware = express.static(path.join(__dirname, "/dist")); // absolute or relative to CWD

// history mode setting..
app.use(staticFileMiddleware);
app.use(history());
app.use(staticFileMiddleware); // `app.use(staticFileMiddleware)` is included twice for history mode
app.get("/", function(req, res) {
  res.render(path.join(__dirname, "/dist/index.html"));
});

// Middlewares
app.use(express.static("dist")); // absolute or relative to CWD
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// CORS 설정
app.use(cors());

app.get("/", function(req, res) {
  res.render(path.join(__dirname, "/dist/index.html"));
});

// JWT add.
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // res.header("Access-Control-Allow-Credentials", true);
  next();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  // res.status(404).send("DAMN! 404 NOT FOUND... YOU MAD?");
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  console.error("app error:" + err.stack.split("at"), req.baseUrl, req.originalUrl);

  let code = err.status || 500;

  // render the error page
  res.status(code);

  if (code === 404) {
    return res.render("index", { error: err }); // 404 는 index 로 처리.
  } else {
    return res.json({
      message: err.message,
      error: err,
    });
  }
});

//
const host = process.env.VUE_APP_HOST || "localhost";
const port = process.env.VUE_APP_PORT || 8083;

app.listen(port, host, () => {
  console.info("========== node express server started =============");
  console.info("Service Mode", process.env.VUE_APP_MODE);
  console.info("Listening on " + host + ":" + port);
  console.info("==================================================== \n");
});

/**
 * Make node.js not exit on error
 */
process.on("uncaughtException", (err) => {
  console.error(chalk.red("==> Caught exception: " + JSON.stringify(err)));
});
