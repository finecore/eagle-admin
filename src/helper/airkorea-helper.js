import proj4 from "proj4";
import _ from "lodash";

/**
 * Airkorea Helper Class.
 */
class AirkoreaHelper {
  constructor() {
    this.ILO_LIST = [0, 51, 101, 251];
    this.IHI_LIST = [50, 100, 250, 500];
    this.BPLO_LIST = {
      so2: [0, 0.021, 0.051, 0.151], // 아황산가스 농도 (ppm)
      co: [0, 2.01, 9.01, 15.01], // 일산화 탄소 농도. (ppm)
      o3: [0, 0.031, 0.091, 0.151], // 오존 농도 (ppm)
      no2: [0, 0.031, 0.061, 0.201], // 이산화 질소 농도. (ppm)
      pm10: [0, 31, 81, 151], // 미세먼지 농도. (㎍/㎥)
      pm25: [0, 16, 36, 76] // 초 미세먼지 농도. (㎍/㎥)
    };
    this.BPHI_LIST = {
      so2: [0.02, 0.05, 0.15, 1], // 아황산가스 농도
      co: [2, 9, 15, 50], // 일산화 탄소 농도.
      o3: [0.03, 0.09, 0.15, 0.6], // 오존 농도
      no2: [0.03, 0.06, 0.2, 2], // 이산화 질소 농도.
      pm10: [30, 80, 150, 600], // 미세먼지 농도.
      pm25: [15, 35, 75, 500] // 초 미세먼지 농도.
    };
  }

  max(name) {
    return this.BPHI_LIST[name][3];
  }

  /*
    지수 산출방법
    6개 대기오염물질별로 통합대기환경지수 점수를 산정하며 가장 높은 점수를 통합 지수값으로 사용
    산출된 각각의 오염물질별 지수점수가 '나쁨'이상의 등급이 2개 물질 이상일 경우 통합지수값에 가산점을 부여
    1개일 경우 : 점수가 가장 높은 지수점수를 통합지수로 사용
    2개일 경우 : 가장 높은 점수가 나온 오염물질을 영향 오염물질로 표시하고 그 오염물질의 점수에 50점을 가산
    3개 이상일 경우 : 가장 높은 점수가 나온 오염물질을 영향 오염물질로 표시하고 그 오염물질의 점수에 75점 가산
    통합대기환경지수는 0에서 500까지의 지수를 4단계로 나누어 점수가 커질수록 대기상태가 좋지 않음을 나타냄

    Ip = (IHI - ILO)/(BPHI - BPLO)*(Cp-BPLO)+ILO :
    Ip = 대상 오염물질의 대기지수점수,
    Cp = 대상오염물질의 대기 중 농도,
    BPHI = 대상 오염물질의 오염도 해당 구간에 대한 최고 오염도,
    BPLO = 대상 오염물질의 오염도 해당 구간에 대한 최저 오염도,
    IHI = BPHI에 해당하는 지수값(구간 최고 지수값),
    ILO = BPLO에 해당하는 지수값(구간 최저 지수값)
    Ip = 대상 오염물질의 대기지수 점수
    Cp = 대상 오염물질의 대기중 농도
    BPHI = 대상 오염물질의 오염도 해당 구간에 대한 최고 오염도
    BPLO = 대상 오염물질의 오염도 해당 구간에 대한 최저 오염도
    IHI = BPHI에 해당하는 지수값(구간 최고 지수값)
    ILO = BPLO에 해당하는 지수값(구간 최저 지수값)

    * 측정된 농도값(Cp)이 정의된 농도값 (BPHI)를 초과하는 경우에 BPHI 값은 매우나쁨의 BPHI값으로 갈음한다.

    주1) 미세먼지 PM10 24hr은 미세먼지 PM10 24시간 예측 이동평균임.
    주2) 초미세먼지 PM2.5 24hr은 초미세먼지 PM2.5 24시간 예측 이동평균임.
    */

  grade(name, Cp) {
    let bplo = this.BPLO_LIST[name];
    let bphi = this.BPHI_LIST[name];
    var max = this.BPHI_LIST[name][3];

    let score = 0;
    let grade = 4;

    // 지수 산출.
    _.map(bplo, (BPLO, i) => {
      let BPHI = bphi[i];

      let ILO = this.ILO_LIST[i];
      let IHI = this.IHI_LIST[i];

      if (Cp >= BPLO && Cp < BPHI) {
        score = ((IHI - ILO) / (BPHI - BPLO)) * (Cp - BPLO) + ILO;

        // console.log("- score", score, { Cp, IHI, ILO, BPHI, BPLO });

        return false;
      }
    });

    // 구간 Grad
    _.map(this.ILO_LIST, (ILO, i) => {
      let IHI = this.IHI_LIST[i];

      if (score >= ILO && score < IHI) {
        grade = i + 1;
        // console.log("- grade", grade);
        return false;
      }
    });

    return {
      grade,
      score,
      name: grade === 1 ? "gGood" : grade === 2 ? "gNormal" : grade === 3 ? "gBad" : "gWorst",
      max
    };
  }

  // 경/위도 -> TM 좌표 변환.
  gpsToTm(lon, lat) {
    var firstProjection = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"; // from
    var secondProjection =
      "+proj=tmerc +lat_0=38 +lon_0=127 +k=1 +x_0=200000 +y_0=500000 +ellps=bessel +units=m +no_defs"; // to

    var posTM = proj4(firstProjection, secondProjection, [lon, lat]);
    console.log("WGS84 경위도 -> TM : ", posTM);

    return posTM;
  }
}

export default AirkoreaHelper;
