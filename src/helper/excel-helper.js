const excel = require("excel4node");
const _ = require("lodash");

function setSheet(wb, sectionTitle, rows) {
  let sheet = wb.addWorksheet(sectionTitle);
  let titleStyle = setStyle(wb, "title");
  let contentStyle = setStyle(wb, "contents");

  let col = 1;

  console.log("- setSheet", sectionTitle, rows);

  if (!rows) return;

  let rowKeys = Object.keys(rows[0]);

  // 타이틀.
  for (let idx in rowKeys) {
    let width = 10;

    let row = rowKeys[idx];

    console.log(row);

    switch (row) {
      case "reg_date":
        width = 17;
        break;
      case "user_id":
        width = 16;
        break;
    }
    sheet.column(col).setWidth(width);
    sheet
      .cell(1, col++)
      .string(row)
      .style(titleStyle);
  }

  // 데이터.
  let row = 2;
  for (let idx in rows) {
    // console.log( rows[idx] );
    let col = 1;
    for (let key in rowKeys) {
      // console.log( 'data ', rows[idx][rowKeys[key]] );
      setInputText(sheet, row, col, rows[idx][rowKeys[key]], contentStyle);
      col++;
    }
    row++;
  }
}

function setStyle(wb, type) {
  let border = {
    top: { style: "thin" },
    bottom: { style: "thin" },
    left: { style: "thin" },
    right: { style: "thin" },
  };
  let alignment = { horizontal: "center" };
  let family = "decorative";
  let fillType = "none";
  let font = {};
  let fill = {};

  if (type === "title") {
    font = {
      bold: true,
      size: 10,
    };
    alignment = { horizontal: "center" };
    fill = {
      patternType: "solid",
      fgColor: "#F8F5EE",
    };
    fillType = "pattern";
  } else if (type === "contents") {
    font = {
      size: 9,
      wrapText: true,
    };
    alignment = { horizontal: "left" };
  } else if (type === "center") {
    font = { size: 9 };
    alignment = { horizontal: "center" };
  }

  font.family = family;
  fill.type = fillType;
  return wb.createStyle({
    border: border,
    font: font,
    alignment: alignment,
    fill: fill,
  });
}

function setInputText(sheet, row, col, text, style) {
  sheet.cell(row, col).style(style);

  console.log(row, " ", col);

  if (!text || text === "" || typeof text === "undefined" || text === undefined || text === "undefined") {
    return sheet.cell(row, col).string(" ");
  } else {
    // eslint-disable-next-line no-control-regex
    text = text.toString().replace(/[\x00-\x09\x0B-\x1F]/gi, "");
    sheet.cell(row, col).string(text);
  }
}

const makeExcel = function(title, name, json, res, cb) {
  let wb = new excel.Workbook();

  setSheet(wb, title, json);

  let filename = name + ".xlsx";

  wb.write(filename, res, function(err) {
    if (err) return cb(err);
    cb(null, filename);
  });
};

module.exports = makeExcel;
