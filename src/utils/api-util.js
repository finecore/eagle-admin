import axios from "axios";
import { SET_API_ERR } from "@/store/mutation_types";
import router from "@/router";
import moment from "moment";

const api = {
  // error request.
  errorRequest: (dispatch, err) => {
    console.log("- errorRequest", err, "\n err.response", err.response);

    dispatch("hideLoader", null, { root: true });

    let error = {
      code: "Network Error",
      message: "HttpRequest ERR_CONNECTION_REFUSED",
    };

    if (err.response) {
      const comerr = err.response.data.common ? err.response.data.common.error : {};

      error = {
        code: err.response.status,
        message: err.response.statusText,
        detail: comerr.message,
      };
    } else if (err) {
      const errs = String(err).split(":");
      error = {
        type: SET_API_ERR,
        code: errs[0] === "TypeError" ? 401 : 400,
        message: errs[1] ? errs[1] : err,
      };
    }

    dispatch("setApiErr", error, { root: true });
  },

  // check response.
  checkResponse: (dispatch, res) => {
    // console.log("- checkResponse", res);

    dispatch("hideLoader", null, { root: true });

    if (res.status !== 200) {
      const error = {
        type: SET_API_ERR,
        code: res.statue,
        message: res.data.message,
        detail: res.statusText,
      };

      dispatch("setApiErr", error, { root: true });
    } else {
      const { common, body } = res.data;
      let { success, error } = common;

      if (!success) {
        // to array.
        if (error.detail && !(error.detail instanceof Array)) error.detail = [error.detail];
        if (res.status !== 200 && !error.detail) error.detail.push(res.statusText);
        dispatch("setApiErr", error, { root: true });
      }
      return { common, body }; // return object.
    }
  },
};

// Ajax As Axios
const ajax = axios.create({
  baseURL: "/",
});

ajax.CancelToken = axios.CancelToken;
ajax.isCancel = axios.isCancel;

// Add a request interceptor
ajax.interceptors.request.use(
  (config) => {
    let { headers, url, method, dispatch, data, loading = true } = config;

    headers["channel"] = "admin"; // 채널 설정.

    let token = sessionStorage.getItem("token");
    if (token) headers["x-access-token"] = token;

    let uuid = sessionStorage.getItem("uuid");
    if (uuid) config.headers["uuid"] = uuid;

    // console.log("- config ", config, loading);

    // 로그아웃 시.
    if (url.indexOf("login") === -1 && (!sessionStorage.getItem("user") || !sessionStorage.getItem("token"))) {
      console.log("- 로그아웃 ", config.dispatch);

      if (dispatch) {
        dispatch(
          "setLogout",
          {
            message: "로그아웃 되었습니다.",
            detail: "세션이 만료 되어 로그아웃 되었습니다.",
          },
          { root: true }
        );
        dispatch("logout", {}, { root: true });
      }
      router.push("login");
      return config;
    }

    config.url = url;
    config.time = new Date();

    console.log(">>>>>> axios request >>>>>>\n %c" + method.toUpperCase(), "background: #222; color: #bada55", moment(config.time).format("mm:ss:SSS"), url, "\ndata:", data);

    // 로딩바 보이기.
    if (dispatch && loading) {
      // 화면 블러킹 처리(로딩바는 안보임)
      dispatch("showLoader", { type: "" }, { root: true });

      setTimeout(() => {
        // 1 초 이상 걸리는 거래만 로딩바 보임.
        if (!config.readed) dispatch("showLoader", { type: "square" }, { root: true });
      }, 1000);
    }

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// Add a response interceptor
ajax.interceptors.response.use(
  (response) => {
    let diff = moment.utc(moment(new Date(), "DD/MM/YYYY HH:mm:ss:SSS").diff(moment(response.config.time, "DD/MM/YYYY HH:mm:ss:SSS"))).format("ss:SSS");

    console.log(
      "<<<<<< axios response <<<<<<\n %c" + response.config.method.toUpperCase() + "%c  %c " + diff + " ",
      "background: #f66; color: #fff",
      "background: #fff; color: #fff",
      "background: #4b49d7; color: #fff",
      response.config.url,
      "(" + moment(response.config.time).format("mm:ss:SSS") + " ~ " + moment(new Date()).format("mm:ss:SSS") + ")",
      "\ndata:",
      response.data
    );

    // 로딩바 숨기기
    if (response.config.dispatch) response.config.dispatch("hideLoader", {}, { root: true });

    // 로드 완료.
    response.config.readed = true;

    return response;
  },
  function(error) {
    return Promise.reject(error);
  }
);

export { api, ajax };
