import _ from "lodash";

export const dataMerge = ({ list, rows, order }) => {
  // console.log("- dataMerge", { list, rows, order });

  let start = rows.length ? _.findIndex(list, { seq: rows[0].seq }) : -1;
  let end = start + rows.length;

  console.log("- before list length", list.length);

  if (start > -1) {
    console.log("- data replace", { start, end });

    // 기존 목록에 있다면 업데이트
    let items = list.slice(0, start);
    items = items.concat(rows);
    items = items.concat(list.slice(end));
    list = items;
    // console.log("- items", list[start].hp, rows[0].hp);
  } else {
    list = list.concat(rows); // 기존 목록에 추가.
  }

  console.log("- after list length", list.length);

  if (list.length) {
    if (order) list = _.orderBy(list, order.target, order.dir);

    _.each(list, (v, k) => (v.idx = k + 1));
  }

  return _.cloneDeep(list);
};

export const dataFilter = (options) => {
  // console.log("- dataFilter", { option });

  let f = "";

  _.map(options, (option, k) => {
    let { name, value, op } = option;
    // console.log("- option", { name, value, op });
    if (value !== "") {
      if (f) f += "|";
      f += `${name}:${op}:${value}`;
    }
  });

  // console.log("- dataFilter", f);

  return f;
};
