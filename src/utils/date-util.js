import moment from "moment";

const date = {
  init: () => {
    // locale:ko를 등록한다. 한번 지정하면 자동적으로 사용된다.
    moment.updateLocale("ko", {
      weekdays: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"],
      weekdaysShort: ["일", "월", "화", "수", "목", "금", "토"],
    });

    // 한국어로 출력
    moment().format("MM/DD (ddd) dddd"); // => 07/12 (수) 수요일

    // 다시 영어로 출력하고 싶을 때
    moment()
      .locale("en")
      .format("MM/DD (ddd) dddd"); // => 07/12 (Wed) Wednesday

    //suppress showing the deprecation warning
    // moment.suppressDeprecationWarnings = true;
  },

  // diff date
  diff: (from, to) => {
    // console.log("- date util diff ", from, to);
    var date1 = moment(from);
    var date2 = moment(to);

    // var diff = moment.utc(moment(date2, "DD/MM/YYYY HH:mm:ss").diff(moment(date1, "DD/MM/YYYY HH:mm:ss")));

    var ms = moment(date2, "DD/MM/YYYY HH:mm:ss").diff(moment(date1, "DD/MM/YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var hh = Math.floor(d.asHours());
    var mm = moment.utc(ms).format("mm");
    var ss = moment.utc(ms).format("ss");

    // console.log("- diff", { hh, mm, ss });

    return {
      diff: ms,
      hh,
      mm,
      ss,
    };
  },
};

date.init();

export default date;
