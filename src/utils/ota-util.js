import moment from "moment";
import _ from "lodash";

import format from "./format-util";
import { keyToValue, keyCodes } from "@/constants/key-map";

const ota = {
  getCode: (text, type) => {
    let codes = keyCodes("room_reserv", type);
    let code = null;

    console.log("- getCode", { text, type });

    _.map(codes, (v) => {
      // console.log("- ota_code text", v.text);
      if (text.indexOf(v.text) > -1) {
        // console.log("- ota_code value", v.value);
        code = v.value;
        return false;
      }
    });

    return code;
  },

  // 객실 타입 조회.
  getRoomType: async (store, place_id) => {
    // console.log("- getRoomType", place_id);
    return store.dispatch(`roomType/getList`, { place_id }).then((res) => {
      // console.log("- res", res);
      return res;
    });
  },

  parse: async (store, otaSubscribePlace, mms_no) => {
    let { MO_KEY, CONTENT } = mms_no;

    const ota_code = ota.getCode(CONTENT, "ota_code");

    let reserv = {
      place_id: null,
      room_type_id: 0,
      room_id: null,
      ota_code,
      ota_type: 2, // OTA 예약 형태 (1:OTA 자동예약, 2: OTA 수동 예약)
      mms_mo_num: MO_KEY, // MMS_NO 참조 num
      reserv_num: null,
      stay_type: 1, // 숙박
      state: "A",
      name: null,
      hp: null,
      check_in_exp: null,
      check_out_exp: null,
      reserv_date: moment().format("YYYY-MM-DD HH:mm"),
      room_fee: 0, // 숙박 기본 요금
      reserv_fee: 0, // 예약 할인 요금
      prepay_ota_amt: 0,
      prepay_cash_amt: 0,
      prepay_card_amt: 0,
      visit_type: 1,
      memo: "",
    };

    let line = CONTENT.split(/\r\n/gi);

    console.log("- ota parse", { ota_code, line });

    let promise = null;

    // 야놀자
    if (ota_code === 1) {
      let place = _.find(otaSubscribePlace, { place_name: line[2] }) || {};

      console.log("- place", place.place_id);

      reserv.place_id = place.place_id;
      reserv.stay_type = /[숙박]/.test(line[1]) ? 1 : 2;
      reserv.reserv_num = line[3];

      if (place.place_id) {
        promise = ota.getRoomType(store, place.place_id).then((roomTypes) => {
          let roomType = _.find(roomTypes, { name: line[5] }) || {};
          reserv.room_type_id = roomType.id;

          reserv.room_fee = 0;
          reserv.reserv_fee = 0;
          reserv.prepay_ota_amt = Number(line[6].replace(/[^\d]/g, ""));

          let customer = line[8].split(/\//);

          reserv.name = _.trim(customer[0]);
          reserv.hp = _.trim(customer[1]);

          let date1 = line[9].match(/(\d+)/g);
          let date2 = line[10].match(/(\d+)/g);

          reserv.check_in_exp = `${date1[0]}-${date1[1]}-${date1[2]} ${date1[3]}:${date1[4]}`;
          reserv.check_out_exp = `${date2[0]}-${date2[1]}-${date2[2]} ${date2[3]}:${date2[4]}`;

          reserv.visit_type = /[도보방문]/.test(line[11]) ? 1 : 2;

          return reserv;
        });
      }
    }
    // 여기어때
    else if (ota_code === 2) {
    }
    // 네이버
    else if (ota_code === 3) {
    }

    return promise;
  },

  ota_yanolja: (message, ota) => {},
};

export default ota;
