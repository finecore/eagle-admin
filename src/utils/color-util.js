const color = {
  colors: [
    '#eeeeee',
    '#FFC300',
    '#008080',
    '#DBDF00',
    '#A4DD00',
    '#68CCCA',
    '#73D8FF',
    '#AEA1FF',
    '#FDA1FF',
    '#808080',
    '#D33115',
    '#E27300',
    '#FCC400',
    '#B0BC00',
    '#68BC00',
    '#16A5A5',
    '#009CE0',
    '#7B64FF',
    '#FA28FF',
    '#B3B3B3',
    '#9F0500',
    '#C45100',
    '#FB9E00',
    '#FFcefc',
    '#194D33',
    '#2a08a7b7'
  ],

  get: idx => {
    return color.colors[idx] || color.colors[0];
  },

  random: () => {
    const idx = Math.floor(Math.random() * this.colors.length);
    return this.colors[idx];
  }
};

export default color;
