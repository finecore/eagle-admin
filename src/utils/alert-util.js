import Swal from "sweetalert2";
import _ from "lodash";
import moment from "moment";

const ComAlert = ({ icon, type, title, text }, callback) => {
  Swal.fire({
    type: icon || (type === "경고" ? "warning" : "info"),
    title: title || type,
    html: text || "",
    confirmButtonColor: "#3085d6",
    confirmButtonText: "확인",
  }).then((result) => {
    callback();
  });
};

const ComConfirm = ({ icon, type, title, text }, callback, cancel) => {
  Swal.fire({
    type: icon || type === "삭제" ? "warning" : "info",
    title: title || (type === "삭제" ? "선택 항목을" : "정보를") + ` ${type} 하시겠습니까?`,
    html: text || (type === "삭제" ? "<font color='red'><b>삭제 시 복구가 불가능 합니다!</b></font>" : ""),
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "예",
    cancelButtonText: "아니오",
  }).then((result) => {
    if (result.value) {
      const resMsg = (success) => {
        if (success)
          Swal.fire({
            title: `${type} 완료!`,
            text: `정상적으로 ${type} 되었습니다.`,
            type: "success",
            timer: 3000,
          });
        else
          Swal.fire({
            title: `${type} 실패!`,
            text: `정보 ${type} 중 오류가 발생 하였습니다.`,
            type: "error",
            timer: 3000,
          });
      };
      callback(resMsg);
    } else {
      if (cancel) cancel();
    }
  });
};

export { ComAlert, ComConfirm };
