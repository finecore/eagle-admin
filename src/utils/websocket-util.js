class WebSocketUtil {
  constructor(store) {
    this.host = process.env.VUE_APP_ADM_SOCKET_HOST;
    this.port = process.env.VUE_APP_ADM_SOCKET_PORT;
    this.socket = null;
    this.status = "disconnected";
    this.store = store;
    this.timer = null;

    console.log("- WebSocketUtil", this.host, this.port);
  }

  connect() {
    // if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    this.socket = new WebSocket(`ws://${this.host}:${this.port}`);

    console.log("- WebSocketUtil connect ", this.socket);

    this.socket.onopen = () => {
      this.status = "connected";
      console.log("- WebSocketUtil onopen ", this.status);

      if (this.timer) clearInterval(this.timer);

      this.timer = setInterval(() => {
        const {
          auth: { auth },
          place: { list }, // 관리 업소 목록.
        } = this.store.state;

        // console.log("- WebSocketUtil onopen ", { auth, list });

        // 웹소켓에 사용자 매핑(Socket Server)
        if (auth && list) {
          this.sendMessage("JOIN_REQUESTED", {
            channel: "admin",
            user: auth,
            user_place: list,
          });
        }
      }, 1000);

      // websocket data receive.
      this.socket.onmessage = ({ data }) => {
        const message = data ? JSON.parse(data) : {};
        const { websocket = true, type = "", payload, metadata } = message;

        // console.log("%c -> websocket receive message ", "background: #222; color: #bada55", { type, payload });

        if (type) {
          console.log("%c -> websocket receive message ", "background: #222; color: #bada55", { type, payload });

          let action = { websocket, type, ...payload, metadata }; // ...payload : reducer action 구조분해 할당.
          this.store.commit(type, action);
        }
      };

      // websocket data send.
      this.store.subscribe((mutation) => {
        if (mutation.type === "UPDATE_DATA") {
          this.socket.emit("update", mutation.payload);
        }
      });
    };

    this.socket.onclose = () => {
      this.disconnect();

      setTimeout(() => {
        this.connect();
      }, 2000);
    };

    this.socket.onerror = (err) => {
      console.error("- WebSocketUtil onerror ", err);
    };
  }

  disconnect() {
    this.socket.close();
    this.status = "disconnected";
    console.error("----> WebSocketUtil disconnected  ");
  }

  sendMessage(type, payload) {
    if (this.status !== "connected") {
      console.error("----> WebSocketUtil disconnected  ");
      return;
    }
    console.log("----> WebSocketUtil send  ", type, payload);

    this.socket.send(JSON.stringify({ type, payload }));

    if (this.timer) clearInterval(this.timer);
  }
}

export default WebSocketUtil;
