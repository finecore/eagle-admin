import axios from "axios";
import { mapGetters, mapActions } from "vuex";
import { ComConfirm, ComAlert } from "@/utils/alert-util";
import _ from "lodash";
import moment from "moment";

const loadingImg = require("@/assets/image/morph_dribbble.gif");

const HOST = process.env.VUE_APP_API_SERVER_HOST;
const PORT = process.env.VUE_APP_API_SERVER_PORT;
const HOST_URL = HOST + ":" + PORT;

const fileFetch = (url, method, token, callback) => {
  const axiosConfig = {
    method: method,
    url: url,
    responseType: "blob",
    headers: { channel: "admin", token },
    params: {},
  };

  try {
    const premise = axios(axiosConfig).then((response) => {
      console.log("- fileFetch", response);

      callback(response.data);
    });
  } catch (err) {
    console.error("Error on force file download process. Try again.");
  }
};

const fileUpload = (url, file, path, name, token) => {
  return new Promise(function(resolve, reject) {
    const formData = new FormData();

    formData.enctype = "multipart/form-data";

    formData.append("path", path);
    formData.append("name", name);

    formData.append("file", file, name || file.name); // 파일은 마지막에 append 해야 함.

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url);

    xhr.setRequestHeader("channel", "admin");
    xhr.setRequestHeader("token", token);

    xhr.upload.onprogress = (e) => {
      console.log("- onprogress", e.lengthComputable, e.loaded, e.total);
    };

    xhr.onreadystatechange = function() {
      console.log("- onreadystatechange", xhr.readyState, xhr.status);
      if (xhr.status === 200) {
        if (xhr.readyState === 4) {
          let response = JSON.parse(xhr.responseText);

          console.log("- onload", response);

          const {
            common: { success, error },
            body: { info, file },
          } = response;

          if (!success || !file) {
            if (error.detail && !(error.detail instanceof Array)) error.detail = [error.detail];
            console.error("- upload error", error);
          } else {
            const { path, name, originalname, size, mimetype } = file;
            console.log("- upload file", file);
          }
          resolve(file);
        }
      } else {
        console.error("- upload error", xhr.status);
        reject(xhr.status);
      }
    };

    xhr.send(formData);
  });
};

const fileDownload = (url, saveName, token) => {
  const axiosConfig = {
    method: "get",
    url: url,
    responseType: "blob",
    headers: { channel: "admin", token },
    params: {},
  };

  console.log("- fileDownload", axiosConfig);

  const promise = axios(axiosConfig)
    .then((response) => {
      console.log("- fileDownload response", response);

      var fileURL = window.URL.createObjectURL(new Blob([response.data]));
      var fileLink = document.createElement("a");

      fileLink.href = fileURL;
      fileLink.setAttribute("download", saveName);
      document.body.appendChild(fileLink);

      fileLink.click();
    })
    .catch((error) => {
      console.error("- fileDownload error", error);
    });

  return promise;
};

const fileDelete = (id, token) => {
  let url = HOST_URL + "/file" + id;

  const axiosConfig = {
    method: "delete",
    url: url,
    headers: { channel: "web", token },
  };

  return axios(axiosConfig).then((response) => {
    console.log(response);
  });
};

//  이미지만 삭제 한다.
const imageRemove = (path, name, token) => {
  let url = HOST_URL + "/image/" + path + "/" + name;

  const axiosConfig = {
    method: "delete",
    url: url,
    headers: { channel: "web", token },
  };

  console.log("- imageRemove", axiosConfig);

  return axios(axiosConfig).then((response) => {
    console.log(response);
  });
};

// Image upload handler
const imageHandler = (quill, path, store) => {
  const input = document.createElement("input");

  input.setAttribute("type", "file");
  input.setAttribute("accept", "image/*");
  input.click();

  input.onchange = async () => {
    const file = input.files[0];

    // Save current cursor state
    const range = quill.getSelection(true);

    // Insert temporary loading placeholder image
    quill.insertEmbed(range.index, "image", loadingImg);

    const { auth, token } = store.getters;

    // Move cursor to right side of image (easier to continue typing)
    quill.setSelection(range.index + 1);

    let date = moment().format("YYYYMMDDHHmmssSSS");
    let name = date + "_" + file.name;

    let url = HOST_URL + "/upload";

    console.log("- imageHandler", quill, { url, name, token });

    // API post, returns image location as string e.g. 'http://www.example.com/images/foo.png'
    fileUpload(url, file, path, name, token).then((file) => {
      console.log("- res file", file);

      if (file) {
        let fpath = url + "/" + path + "/" + name;

        // Remove placeholder image
        quill.deleteText(range.index, 1);

        // Insert uploaded image
        quill.insertEmbed(range.index, "image", fpath);

        // Move range
        quill.setSelection(range.index + 1);
      }
    });
  };
};

export { fileFetch, fileUpload, fileDownload, fileDelete, imageRemove, imageHandler };
