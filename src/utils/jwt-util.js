const jwt = require("jsonwebtoken");
const { ERROR } = require("../constants/constants");

// JWT Secreet key.
const JWT_SECRET = "Eagle@$Jwt!#Secreet%^Key&";
const JWT_EXPIRE = 60 * 60 * 24;

/**
 * JWT Utils.
 */
const verify = (token, callback) => {
  if (!token) callback(ERROR.NO_CERTIFICATION, null);
  // jwt 에서 인증 정보 추출.
  else {
    jwt.verify(token, JWT_SECRET, (_err, _payload) => {
      console.log("jwt verify ", _err, JSON.stringify(_payload));
      callback(_err, _payload);
    });
  }
};

const sign = (payload, callback, expires) => {
  // 유효 기간.
  var options = {
    expiresIn: expires || JWT_EXPIRE
  };

  console.log("jwt sign ", JSON.stringify(payload), JSON.stringify(options));

  jwt.sign(payload, JWT_SECRET, options, (_err, _token) => {
    console.log("jwt token " + _err, _token);
    callback(_err, _token);
  });
};

module.exports = { verify, sign };
