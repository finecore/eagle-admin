import Swal from "sweetalert2";
import _ from "lodash";
import moment from "moment";

const CheckSubscribe = ({ subscribes, placeSubscribes, code, isAlert = true }) => {
  let email = "service@icrew.kr";
  let tel = "1600-5356";

  if (!subscribes || !placeSubscribes) {
    console.log("- CheckSubscribe no props", { subscribes, placeSubscribes });
    return false;
  }

  const sub = _.find(subscribes, { code });
  console.log("- CheckSubscribe subscribes", { code, sub });

  // 오류 방지 차원에서 없는 구독이나 사용 안하는 구독일 때는 true 반환.
  if (!sub || sub.use_yn === "N") {
    console.log("- CheckSubscribe error subscribes", { code, sub });

    // Swal.fire({
    //   icon: "warning",
    //   title: "구독 알림",
    //   html: `구독 코드 ${code} 는 없는 코드 입니다.<br/><br/>이메일(${email}) 이나 콜센터(${tel})로 문의 바랍니다.`,
    //   confirmButtonText: "확인",
    // }).then(result => {
    //   if (callback) callback(false);
    // });
    // return false;
    return true;
  }

  const _sub = _.find(placeSubscribes, { subscribe_id: sub.id });
  console.log("- CheckSubscribe place subscribes", { code, _sub });

  let valid = true;
  let msg = "";

  if (!_sub) {
    msg = `해당 서비스는 [<font color="#9508e6">${sub.name}</font>] 구독 후 이용 가능합니다.
        <br/><br/>구독신청은 아래 이메일이나 콜센터로 문의 해주세요.<br/><br/>E-mail: ${email}<br/>Tel: ${tel}`;
    valid = false;
  } else {
    // 구독 시작 일자.
    if (moment(_sub.begin_date).isAfter(moment())) {
      msg = `[<font color="#9508e6">${sub.name}</font>] 구독 기간이 아닙니다.
        <br/><br/>구독 기간 ${moment(_sub.begin_date).format("YYYY-MM-DD")} ~ ${moment(_sub.end_date).format("YYYY-MM-DD")}
        <br/><br/>구독 기간을 확인해 주세요.
        <br/><br/>구독신청은 아래 이메일이나 콜센터로 문의 해주세요.<br/><br/>E-mail: ${email}<br/>Tel: ${tel}`;
      valid = false;
    }

    // 무료 서비스 상태.
    if (_sub.valid_yn === 0) {
      // 무료 서비스 기간 지났다면.
      if (moment(_sub.trial_end_date).isBefore(moment())) {
        msg = `[<font color="#9508e6">${sub.name}</font>] 무료구독 기간이 <font color="#f00000">종료</font> 되었습니다.
        <br/><br/>구독신청은 아래 이메일이나 콜센터로 문의 해주세요.<br/><br/>E-mail: ${email}<br/>Tel: ${tel}`;
        valid = false;
      }
    }
    // 정상 서비스 상태.
    else if (_sub.valid_yn === 1) {
      // 구독 만료 일자 지났다면.
      if (moment(_sub.end_date).isBefore(moment())) {
        msg = `[<font color="#9508e6">${sub.name}</font>] 구독 기간이 <font color="#f00000">만료</font> 되었습니다.
        <br/><br/>구독 기간 ${moment(_sub.begin_date).format("YYYY-MM-DD")} ~ ${moment(_sub.end_date).format("YYYY-MM-DD")}
        <br/><br/>구독 기간을 연장해 주세요.
        <br/><br/>구독신청은 아래 이메일이나 콜센터로 문의 해주세요.<br/><br/>E-mail: ${email}<br/>Tel: ${tel}`;
        valid = false;
      }
    }
    // 서비스 중지
    else {
      msg = `[<font color="#9508e6">${sub.name}</font>] 구독 서비스가 <font color="#f00000">중지</font> 되었습니다.
        <br/><br/>구독 기간 ${moment(_sub.begin_date).format("YYYY-MM-DD")} ~ ${moment(_sub.end_date).format("YYYY-MM-DD")}
        <br/><br/>구독신청은 아래 이메일이나 콜센터로 문의 해주세요.<br/><br/>E-mail: ${email}<br/>Tel: ${tel}`;
      valid = false;
    }
  }

  if (isAlert && msg) {
    Swal.fire({
      icon: "info",
      title: "구독 알림",
      html: msg,
      confirmButtonText: "확인",
    });
  }

  return valid;
};

const CheckLicense = ({ subscribes, placeSubscribes, code, use_license_copy, callback = () => {} }) => {
  let email = "service@icrew.kr";
  let tel = "1600-5356";

  if (!subscribes || !placeSubscribes) {
    console.log("- CheckLicense no props", { subscribes, placeSubscribes });
    return false;
  }

  const sub = _.find(subscribes, { code });
  console.log("- CheckLicense subscribes", { code, sub });

  // 오류 방지 차원에서 없는 구독이나 사용 안하는 구독일 때는 true 반환.
  if (!sub || sub.use_yn === "N") {
    console.log("- CheckLicense error subscribes", { code, sub });
    return true;
  }

  if (sub.license_yn !== "Y") {
    Swal.fire({
      icon: "warning",
      title: "구독 라이선스 알림",
      html: `구독 코드 ${code} 는 현재 라이선스 사용이 불가 합니다.<br/><br/>이메일(${email}) 이나 콜센터(${tel})로 문의 바랍니다.`,
      confirmButtonText: "확인",
    }).then((result) => {
      let success = false;
      if (callback) callback(success);
    });
    return false;
  }

  const _sub = _.find(placeSubscribes, { subscribe_id: sub.id });

  // 미 구독 시 기본 라이선스 적용.
  let license_copy = Number(sub.default_license);

  if (_sub) {
    console.log("- CheckLicense place subscribes default_license", sub.default_license, "추가 license_copy", _sub.license_copy);
    license_copy = Number(sub.default_license) + Number(_sub.license_copy);
  }

  console.log("- CheckLicense place subscribes", { license_copy, use_license_copy });

  if (license_copy <= Number(use_license_copy)) {
    Swal.fire({
      icon: "error",
      title: "라이선스 사용 알림",
      html: `${sub.name} 사용 초과 입니다.<br/><br/>사용 가능한 라이선스는 총 ${license_copy} Copy 입니다.<br/><br/>[환경설정 > 구독관리] 메뉴에서 <font color="#0000ff">라이선스를 추가 구독</font> 하시거나 <br/>이메일(${email}) 이나 콜센터(${tel})로 문의 바랍니다.`,
      confirmButtonText: "확인",
    }).then((result) => {
      let success = false;
      if (callback) callback(success);
    });

    return false;
  }

  let success = true;
  if (callback) callback(success);

  return license_copy;
};

const CheckServiceGroup = (props, codes) => {
  const { subscribes, placeSubscribes } = props;

  console.log("- CheckServiceGroup codes", codes);
  // console.log("- CheckServiceGroup ", { subscribes, placeSubscribes });

  if (!subscribes || !placeSubscribes) {
    console.log("- CheckServiceGroup no props", { subscribes, placeSubscribes });
    return false;
  }

  let pass = true;

  _.each(codes, (code) => {
    const sub = _.find(subscribes, { code });
    // console.log("- CheckServiceGroup subscribes", { code, sub });

    // 오류 방지 차원에서 없는 구독이나 사용 안하는 구독일 때는 true 반환.
    if (!sub || sub.use_yn === "N") {
      console.log("- CheckServiceGroup error subscribes", { code, sub });
      return true;
    }

    const _sub = _.find(placeSubscribes, { subscribe_id: sub.id });
    // console.log("- CheckServiceGroup place subscribes", { code, _sub });

    if (!_sub) {
      pass = false;
    } else {
      // 구독 시작 일자.
      if (moment(_sub.begin_date).isAfter(moment())) {
        pass = false;
      }

      // 구독 만료 일자.
      if (moment(_sub.end_date).isBefore(moment())) {
        pass = false;
      }

      // 구독 서비스 중지 상태.(무료기간 종료, 요금 미납등)
      if (!_sub.valid_yn) {
        // 무료 서비스 기간.
        if (moment(_sub.trial_end_date).isBefore(moment())) {
          pass = false;
        }
      }
    }

    // console.log("- CheckServiceGroup pass", pass);

    if (!pass) return false;
  });

  return pass;
};

export { CheckSubscribe, CheckLicense, CheckServiceGroup };
