import Vue from "vue";
import VueRouter from "vue-router";
import RouterPrefetch from "vue-router-prefetch";

import App from "./App";
// TIP: change to import router from "./router/starterRouter"; to start with a clean layout
import router from "./router/index";
import store from "./store";

import VeeValidate from "vee-validate";
import ko from "vee-validate/dist/locale/ko.js";

import BlackDashboard from "./plugins/blackDashboard";

import i18n from "./i18n";
import "./registerServiceWorker";

// Styles.
import Vuetify from "vuetify";
import moment from "moment";
import _ from "lodash";
import cx from "classnames";
import "vuetify/dist/vuetify.min.css"; // Ensure you are using css-loader
import format from "./utils/format-util";
import { keyToValue, keyCodes } from "./constants/key-map";

import Quill from "quill";
import VueQuillEditor from "vue-quill-editor";

import { ImageDrop } from "quill-image-drop-module";
import ImageResize from "quill-image-resize-module";

import "quill/dist/quill.core.css"; // import styles
import "quill/dist/quill.snow.css"; // for snow theme
import "quill/dist/quill.bubble.css"; // for bubble theme

Vue.use(VueQuillEditor /* { default global options } */);

Quill.register("modules/imageDrop", ImageDrop);
// Quill.register("modules/imageResize", ImageResize);

Vue.use(BlackDashboard);
Vue.use(VueRouter);
Vue.use(RouterPrefetch);

// vuetify style template.
Vue.use(Vuetify, {
  iconfont: "fa" || "mdi", // fontAwesome, materialDesignIcon
  icons: {}, // Icon alias.
});

// vee-validate
Vue.use(VeeValidate, {
  validity: true,
  locale: "ko",
  dictionary: {
    ko,
  },
});

// 전역 Vue 선언.
Vue.prototype.EventBus = new Vue();
Vue.prototype._ = _;
Vue.prototype.moment = moment;
Vue.prototype.cx = cx;
Vue.prototype.format = format;
Vue.prototype.keyToValue = keyToValue;
Vue.prototype.keyCodes = keyCodes;

moment.updateLocale("ko", {
  weekdays: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"],
  weekdaysShort: ["일", "월", "화", "수", "목", "금", "토"],
});

// local host 여부.
const { hostname } = window.location;
Vue.prototype.isLocalHost = hostname.indexOf("localhost") > -1;
console.log("- hostname", hostname, " isLocalHost", Vue.prototype.isLocalHost);

// Vue.config.productionTip = false; // false로 설정하면 배포에 대한 팁을 출력하지 않습니다.

/* eslint-disable no-new */
new Vue({
  store,
  router,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
