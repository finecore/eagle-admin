import _ from "lodash";
import moment from "moment";

export const codes = {
  com: {
    yesNo: [
      { text: "Yes", value: 1 },
      { text: "No", value: 0 },
    ],
    useYn: [
      { text: "사용", value: "Y" },
      { text: "사용안함", value: "N" },
    ],
    acptYn: [
      { text: "허용", value: "Y" },
      { text: "허용안함", value: "N" },
    ],
    agreeYn: [
      { text: "동의", value: "Y" },
      { text: "동의안함", value: "N" },
    ],
    mailYn: [
      { text: "수신", value: "Y" },
      { text: "수신안함", value: "N" },
    ],
    otaMailYn: [
      { text: "수신", value: "1" },
      { text: "실패만 수신", value: "2" },
      { text: "수신안함", value: "0" },
    ],
    payYn: [
      { text: "유료", value: "Y" },
      { text: "무료", value: "N" },
    ],
    openYn: [
      { text: "비공개", value: 0 },
      { text: "공개", value: 1 },
    ],
    displayYn: [
      { text: "알림", value: 1 },
      { text: "없음", value: 0 },
    ],
    repeatYn: [
      { text: "일간 반복", value: 1 },
      { text: "없음", value: 0 },
    ],
    parseYn: [
      { text: "Yes", value: 1 },
      { text: "No", value: 0 },
    ],
    svcModes: [
      { text: "Air", value: 0 },
      { text: "Pro", value: 1 },
    ],
    pms: [
      { text: "산하정보기술(sanha)", value: "sanha" },
      { text: "신도이디에스(shindo)", value: "shindo" },
      { text: "마이크로닉(micronic)", value: "micronic" },
      { text: "루넷(roonets)", value: "roonets" },
      { text: "무노스(moonos)", value: "moonos" },
      { text: "호텔스토리(hotelstory)", value: "hotelstory" },
      { text: "KT기가지니(gigagenie)", value: "gigagenie" },
    ],
    sidos: [
      { text: "서울", value: 0, full: "서울" },
      { text: "경기", value: 0, full: "경기도" },
      { text: "인천", value: 0, full: "인천" },
      { text: "강원", value: 0, full: "강원도" },
      { text: "충북", value: 0, full: "충청북도" },
      { text: "충남", value: 0, full: "충청남도" },
      { text: "대전", value: 0, full: "대전" },
      { text: "경북", value: 0, full: "경상북도" },
      { text: "경남", value: 0, full: "경상남도" },
      { text: "대구", value: 0, full: "대구" },
      { text: "전북", value: 0, full: "전라북도" },
      { text: "전남", value: 0, full: "전라남도" },
      { text: "광주", value: 0, full: "광주" },
      { text: "울산", value: 0, full: "울산" },
      { text: "부산", value: 0, full: "부산" },
      { text: "제주", value: 0, full: "제주도" },
    ],
  },
  room: {
    state: [
      { value: 0, text: "공실" },
      { value: 1, text: "입실" },
      { value: 2, text: "외출" },
      { value: 3, text: "청소요청" },
      { value: 4, text: "청소중" },
      { value: 5, text: "청소완료" },
      { value: 6, text: "퇴실" },
      { value: 7, text: "입실취소" },
      { value: 8, text: "퇴실취소" },
    ],
  },
  room_state: {
    sale: [
      { value: 0, text: "공실" },
      { value: 1, text: "숙박" },
      { value: 2, text: "대실" },
      { value: 3, text: "장기" },
    ],
    key: [
      { value: 0, text: "NO KEY" },
      { value: 1, text: "고객키 ▼" },
      { value: 2, text: "마스터 ▼" },
      { value: 3, text: "청소키 ▼" },
      { value: 4, text: "고객키 △" },
      { value: 5, text: "마스터 △" },
      { value: 6, text: "청소키 △" },
    ],
    door: [
      { value: 0, text: "문 닫】【힘" },
      { value: 1, text: "문 【열림】" },
    ],
    isc_sale: [
      { value: 0, text: "숙박/대실" },
      { value: 1, text: "X" },
      { value: 2, text: "숙박" },
      { value: 3, text: "대실" },
    ],
    clean: [
      { value: 0, text: "청소 완료" },
      { value: 1, text: "청소 요청" },
    ],
    outing: [
      { value: 0, text: "외출 복귀" },
      { value: 1, text: "외출" },
    ],
    signal: [
      { value: 0, text: "통신 정상" },
      { value: 1, text: "통신 이상" },
    ],
    main_relay: [
      { value: 0, text: "전원 차단" },
      { value: 1, text: "전원 공급" },
    ],
    car_call: [
      { value: 0, text: "배차 없음" },
      { value: 1, text: "배차 요청" },
      { value: 2, text: "배차 완료" },
      { value: 3, text: "배차 취소" },
    ],
    fire: [
      { value: 0, text: "화재 없음" },
      { value: 1, text: "화재 발생" },
    ],
    emerg: [
      { value: 0, text: "긴금 없음" },
      { value: 1, text: "긴급 발생" },
      { value: 2, text: "긴금 종료" },
    ],
  },
  room_state_log: {
    key: [
      { value: "channel", text: "이벤트 발생 채널" },
      { value: "dnd", text: "방해금지" },
      { value: "clean", text: "청소요청" },
      { value: "clean_change_time", text: "청소 요청/완료 시간" },
      { value: "fire", text: "화재발생" },
      { value: "emerg", text: "비상호출" },
      { value: "sale", text: "객실판매" },
      { value: "sale_change_time", text: "입/퇴실/공실 전환 시간" },
      { value: "isc_sale", text: "객실별 자판기 판매 여부" },
      { value: "isc_sale_1", text: "숙박 무인 판매 여부" },
      { value: "isc_sale_2", text: "대실 무인 판매 여부" },
      { value: "isc_sale_3", text: "예약 무인 판매 여부" },
      { value: "key", text: "키 상태" },
      { value: "key_change_time", text: "키값 변동 시간" },
      { value: "outing", text: "외출여부" },
      { value: "signal", text: "통신상태" },
      { value: "theft", text: "도난센서" },
      { value: "emlock", text: "EM LOCK" },
      { value: "door", text: "도어상태" },
      { value: "car_call", text: "차량호출" },
      { value: "airrcon_relay", text: "에어컨릴레이" },
      { value: "main_relay", text: "메인릴레이" },
      { value: "use_auto_power_off", text: "자동 전원 차단 사용 여부(0:안함, 1:사용) " },
      { value: "bath_on_delay", text: "욕실등 ON 지연시간" },
      { value: "num_light", text: "넘버등제어" },
      { value: "chb_led", text: "CHB LED 제어" },
      { value: "car_ss1", text: "차량 SS1" },
      { value: "car_ss2", text: "차량 SS2" },
      { value: "car_ss3", text: "차량 SS3" },
      { value: "shutter", text: "셔터상태" },
      { value: "toll_gate", text: "정산기문" },
      { value: "entrance", text: "입실경로" },
      { value: "air_sensor_no", text: "센서번호" },
      { value: "air_set_temp", text: "설정온도" },
      { value: "air_set_min", text: "온도 설정 최소값" },
      { value: "air_set_max", text: "온도 설정 최대값." },
      { value: "air_temp", text: "현재온도" },
      { value: "air_preheat", text: "예열/예냉상태" },
      { value: "air_heat_type", text: "냉/난방" },
      { value: "air_fan", text: "팬제어" },
      { value: "air_power", text: "에어컨 전원상태" },
      { value: "air_power_type", text: "에어컨 전원 기본 설정" },
      { value: "main_power_type", text: "객실 전원 기본 설정" },
      { value: "light", text: "전등 상태" },
      { value: "dimmer", text: "딤머 상태" },
      { value: "curtain", text: "커튼 상태" },
      { value: "boiler_no", text: "보일러번호" },
      { value: "boiler_set_temp", text: "설정온도" },
      { value: "boiler_temp", text: "현재온도" },
      { value: "boiler_type", text: "냉/난방" },
      { value: "boiler_heating", text: "가동중" },
      { value: "boiler_thermo", text: "실온선택" },
      { value: "boiler_power", text: "전원상태" },
      { value: "notice", text: "객실 표시" },
      { value: "notice_opacity", text: "표시 투명도" },
      { value: "notice_display", text: "객실 표시 여부" },
      { value: "temp_key_1", text: "사용중 온도" },
      { value: "temp_key_2", text: "외출중 온도" },
      { value: "temp_key_3", text: "공실 온도" },
      { value: "temp_key_4", text: "청소중 온도" },
      { value: "temp_key_5", text: "청소대기 온도" },
    ],
  },
  room_sale: {
    state: [
      { value: "A", text: "매출 등록" },
      { value: "B", text: "입실 취소" },
      { value: "C", text: "정산 완료" },
    ],
    channel: [
      { value: "web", text: "프론트" },
      { value: "mobile", text: "모바일" },
      { value: "isc", text: "무인" },
      { value: "device", text: "IDM" },
      { value: "api", text: "자동" },
      { value: "pms", text: "PMS" },
    ],
    stay_type: [
      { value: 0, text: "공실" },
      { value: 1, text: "숙박" },
      { value: 2, text: "대실" },
      { value: 3, text: "장기" },
    ],
  },
  room_interrupt: {
    channel: [
      { value: 1, text: "WEB" },
      { value: 2, text: "CCU" },
      { value: 3, text: "ISC" },
      { value: 4, text: "PAD" },
    ],
  },
  room_reserv: {
    ota_code: [
      { value: 1, text: "야놀자" },
      { value: 2, text: "여기어때" },
      { value: 3, text: "네이버" },
      { value: 4, text: "에어비앤비" },
      { value: 5, text: "호텔나우" },
      { value: 8, text: "기타" },
      { value: 9, text: "직접예약" },
    ],
    stay_type: [
      { value: 1, text: "숙박" },
      { value: 2, text: "대실" },
      { value: 3, text: "장기" },
    ],
    state: [
      { value: "A", text: "정상예약" },
      { value: "B", text: "취소예약" },
      { value: "C", text: "사용완료" },
    ],
    visit_type: [
      { value: 1, text: "도보방문" },
      { value: 2, text: "차량방문" },
      { value: 3, text: "대중교통" },
    ],
  },
  user: {
    type: [
      // { value: 1, text: "아이크루" },
      // { value: 2, text: "대리점" },
      { value: 3, text: "업소" },
    ],
    level: [
      { value: 0, text: "슈퍼관리자" },
      { value: 1, text: "책임자(업주)" },
      { value: 2, text: "관리자(매니저)" },
      { value: 3, text: "근무자(카운터)" },
      { value: 4, text: "메이드(청소원)" },
      { value: 5, text: "PMS" },
      { value: 9, text: "뷰어" },
    ],
    licence_type: [
      { value: 1, text: "고정 접속" },
      { value: 2, text: "유동 접속" },
    ],
  },
  place: {
    type: [
      { value: "M", text: "모텔" },
      { value: "H", text: "호텔" },
      { value: "E", text: "기타" },
    ],
  },
  device: {
    type: [
      { value: "01", text: "IDM" },
      { value: "02", text: "ISG" },
      { value: "10", text: "RPT" },
    ],
    connect: [
      { value: 0, text: "정상" },
      { value: 1, text: "비정상" },
    ],
  },
  notice: {
    type: [
      { value: 1, text: "공지 알림" },
      { value: 2, text: "업데이트 알림" },
    ],
    important: [
      { value: 1, text: "일반" },
      { value: 2, text: "중요" },
    ],
  },
  notice_place: {
    type: [
      { value: 1, text: "공지" },
      { value: 2, text: "메모" },
    ],
    state: [
      { value: 1, text: "등록" },
      { value: 2, text: "확인" },
      { value: 3, text: "처리중" },
      { value: 4, text: "취소" },
      { value: 9, text: "처리완료" },
    ],
  },
  as: {
    type: [
      { value: 1, text: "AS 요청" },
      { value: 2, text: "건의 사항" },
    ],
    state: [
      { value: 1, text: "등록" },
      { value: 2, text: "확인" },
      { value: 3, text: "처리중" },
      { value: 4, text: "취소" },
      { value: 9, text: "처리완료" },
    ],
  },
  model: {
    "01": [
      { value: "01", text: "A01" }, // 369 데몬
      { value: "02", text: "I01" }, // iCrew 데몬
    ],
    "02": [{ value: "01", text: "XA" }],
    10: [{ value: "01", text: "R01" }],
  },
  subscribe: {
    pay_type: [
      { value: "D", text: "일간 결제" },
      { value: "M", text: "월간 결제" },
      { value: "Y", text: "년간 결제" },
    ],
  },
  place_subscribe: {
    applyState: [
      { value: null, text: "신청 상태 선택" },
      { value: 0, text: "신규 신청" },
      { value: 1, text: "변경 신청" },
      { value: 2, text: "구독 완료" },
    ],
    validYn: [
      { text: "무료 서비스", value: 0 },
      { text: "정상 서비스", value: 1 },
      { text: "서비스 중지", value: 2 },
    ],
  },
  mms_mo: {
    status: [
      { value: "I", text: "예약 대기" }, // 미리 예약
      { value: "S", text: "예약 완료" },
      { value: "E", text: "예약 실패" },
      { value: "N", text: "기타" },
    ],
  },
  mail_receiver: {
    type: [
      { value: "01", text: "AS 요청 메일 수신" },
      { value: "02", text: "구독 신청 메일 수신" },
      { value: "03", text: "OTA 자동예약 실패 메일 수신" },
      // { value:"09", text:"오류 메일 수신" }
    ],
  },
  isc_state: {
    key: [
      { value: "state", text: "판매상태" },
      { value: "step", text: "진행단계" },
      { value: "minor_mode", text: "성인인증 모드" },
      { value: "minor_state", text: "성인인증 상태" },
      { value: "minor_auth_type", text: "성인인증 타입" },
      { value: "minor_auto_disable", text: "성인인증 자동복귀" },
      { value: "door", text: "정산기문상태" },
      { value: "motion", text: "동작감지" },
      { value: "rent_sale_count", text: "대실판매객실수" },
      { value: "stay_sale_count", text: "숙박판매객실수" },
      { value: "language", text: "언어" },
      { value: "voice_call", text: "음성지원상태" },
      { value: "setting", text: "자판기설정상태" },
      { value: "start_time", text: "고객조작시작시간" },
      { value: "end_time", text: "고객조작종료시간" },
      { value: "reserv_num", text: "예약번호" },
      { value: "reserv_error_code", text: "예약번호 오류코드" },
      { value: "reserv_error_msg", text: "예약번호 오류메세지" },
      { value: "stay_type", text: "숙박형태" },
      { value: "room_id", text: "객실번호" },
      { value: "pay_amt", text: "결제금액" },
      { value: "pay_type", text: "결제방법" },
      { value: "dspl_signal", text: "좌측 방출기 통신상태" },
      { value: "dspl_state", text: "좌측 방출기 동작상태" },
      { value: "dspl_save_money", text: "좌측 방출기 잔여지폐" },
      { value: "dspl_not_change", text: "좌측 방출기 미방출금" },
      { value: "dspl_change_unit", text: "좌측 방출기 권종" },
      { value: "dspl_change_req_cnt", text: "좌측 방출기 방출 요청 장수" },
      { value: "dspl_change_res_cnt", text: "좌측 방출기 방출 완료 장수" },
      { value: "dspl_error_code", text: "좌측 방출기 에러코드" },
      { value: "dspl_error_msg", text: "좌측 방출기 에러내용" },
      { value: "dspr_signal", text: "우측 방출기 통신상태" },
      { value: "dspr_state", text: "우측 방출기 동작상태" },
      { value: "dspr_save_money", text: "우측 방출기 잔여지폐" },
      { value: "dspr_not_change", text: "우측 방출기 미방출금" },
      { value: "dspr_change_unit", text: "우측 방출기 권종" },
      { value: "dspr_change_req_cnt", text: "우측 방출기 방출 요청 장수" },
      { value: "dspr_change_res_cnt", text: "우측 방출기 방출 완료 장수" },
      { value: "dspr_error_code", text: "우측 방출기 에러코드" },
      { value: "dspr_error_msg", text: "우측 방출기 에러내용" },
      { value: "act_signal", text: "현금입수기 통신상태" },
      { value: "act_state", text: "현금입수기 동작상태" },
      { value: "act_input_fee", text: "현금입수금액합계" },
      { value: "act_input_unit", text: "현금입수권종" },
      { value: "act_error_code", text: "현금입수기 에러코드" },
      { value: "act_error_msg", text: "현금입수기 에러내용" },
      { value: "crd_signal", text: "카드입수기 통신상태" },
      { value: "crd_state", text: "카드입수기 동작상태" },
      { value: "crd_card_no", text: "카드입수기 동작상태" },
      { value: "crd_approval_no", text: "카드승인번호" },
      { value: "crd_type", text: "카드유형" },
      { value: "crd_error_code", text: "카드입수기 에러코드" },
      { value: "crd_error_msg", text: "카드입수기 에러내용" },
      { value: "prt_signal", text: "영수증출력기 통신상태" },
      { value: "prt_state", text: "영수증출력기 동작상태" },
      { value: "prt_error_code", text: "영수증출력기 에러코드" },
      { value: "prt_error_msg", text: "영수증출력기 에러내용" },
    ],
    state: [
      { value: 0, text: "판매 중" },
      { value: 1, text: "판매중지" },
      { value: 2, text: "예약 판매만 가능" },
    ],
    step: [
      { value: 1, text: "판매 대기" },
      { value: 2, text: "성인 인증" },
      { value: 3, text: "예약 확인" },
      { value: 4, text: "객실 선택" },
      { value: 5, text: "객실 확인" },
      { value: 6, text: "결제" },
      { value: 7, text: "결제 확인" },
      { value: 8, text: "영수증 출력" },
    ],
    motion: [
      { value: 0, text: "고객 이동" },
      { value: 1, text: "고객 접근" },
    ],
    minor_mode: [
      { value: 0, text: "성인인증 모드 해제" },
      { value: 1, text: "성인인증 모드 설정" },
    ],
    minor_state: [
      { value: 0, text: " 대기상태" },
      { value: 1, text: "성인인증 진행중" },
      { value: 2, text: "성인인증 완료" },
      { value: 3, text: "성인인증  실패" },
    ],
    minor_auth_type: [
      { value: 0, text: "셀프인증" },
      { value: 1, text: "관제인증" },
    ],
    minor_auto_disable: [
      { value: 0, text: "미사용" },
      { value: 1, text: "사용" },
    ],
    door: [
      { value: 0, text: "정산기 문 닫힘" },
      { value: 1, text: "정산기 문 열림" },
    ],
    setting: [
      { value: 0, text: "환경설정 완료" },
      { value: 1, text: "환경설정 중.." },
    ],
    language: [
      { value: 1, text: "한국어" },
      { value: 2, text: "영어" },
      { value: 3, text: "중국어" },
      { value: 4, text: "일본어" },
    ],
    voice_call: [
      { value: 0, text: "음성지원 대기" },
      { value: 1, text: "음성지원 요청" },
      { value: 2, text: "음성지원 중.." },
      { value: 3, text: "음성지원 완료" },
    ],
    dspl_signal: [
      { value: 0, text: "좌측 방출기 통신 정상" },
      { value: 1, text: "좌측 방출기 통신 단절" },
    ],
    dspr_signal: [
      { value: 0, text: "우측 방출기 통신 정상" },
      { value: 1, text: "우측 방출기 통신 단절" },
    ],
    dspl_state: [
      { value: 0, text: "좌측 방출기 대기 상태" },
      { value: 1, text: "좌측 방출기 방출 동작" },
      { value: 2, text: "좌측 방출기 방출 완료" },
    ],
    dspr_state: [
      { value: 0, text: "우측 방출기 대기 상태" },
      { value: 1, text: "우측 방출기 방출 동작" },
      { value: 2, text: "우측 방출기 방출 완료" },
    ],
    act_signal: [
      { value: 0, text: "현금 입수기 통신 정상", short: "정상" },
      { value: 1, text: "현금 입수기 통신 단절", short: "단절" },
    ],
    act_state: [
      { value: 0, text: "현금 입수 금지 상태", short: "금지" },
      { value: 1, text: "현금 입수 대기 상태", short: "사용" },
    ],
    act_use_type: [
      { value: 1, text: "사용" },
      { value: 0, text: "금지" },
    ],
    crd_signal: [
      { value: 0, text: "카드 입수기 통신 정상", short: "정상" },
      { value: 1, text: "카드 입수기 통신 단절", short: "단절" },
    ],
    crd_type: [
      { value: 1, text: "IC카드" },
      { value: 2, text: "삼성페이" },
    ],
    crd_state: [
      { value: 0, text: "카드 대기 상태" },
      { value: 1, text: "카드 투입 대기" },
      { value: 2, text: "카드 결제 중" },
      { value: 3, text: "카드 제거" },
      { value: 4, text: "카드 결제 성공" },
      { value: 5, text: "카드 결제 실패" },
      { value: 6, text: "카드 사용 중지" },
    ],
    crd_use_type: [
      { value: 0, text: "사용" },
      { value: 1, text: "사용" },
      { value: 2, text: "사용" },
      { value: 3, text: "사용" },
      { value: 4, text: "사용" },
      { value: 5, text: "사용" },
      { value: 6, text: "중지" },
    ],
    key_state: [
      { value: 0, text: "키 토출 대기" },
      { value: 1, text: "키 토출 중" },
      { value: 2, text: "키 토출 완료" },
      { value: 3, text: "키 토출 실패" },
    ],
    prt_signal: [
      { value: 0, text: "영수증 출력기 통신 정상" },
      { value: 1, text: "영수증 출력기 통신 단절" },
    ],
    prt_state: [
      { value: 0, text: "영수증 미출력" },
      { value: 1, text: "영수증 출력" },
    ],
  },
};

export function keyCodes(talble, type) {
  // console.log("--- keyCodes", { talble, type }, codes[talble][type]);

  return codes[talble][type] || [];
}

export function keyToValue(talble, type, value) {
  if (value && value.length === 1 && !isNaN(value)) value = Number(value);

  let code = _.find(codes[talble][type] || [], { value });
  // console.log("--- keyToValue", { talble, type, value }, code, codes[talble][type]);

  if (!code) {
    // console.log("- keyToValue error", { talble, type, value });
    return value;
  }

  return code.text;
}

// 자판기 관제 상태 화면
export function iscDisplayState(iscState) {
  /* 스텝
  1 :판매대기 단계
  2 :성인인증  모드
  3 :예약 확인 단계
  4 :객실 선택 단계 (예약 시 스킵)
  5 :객실 선택 확인 단계 (예약 시 스킵)
  6 :결제 단계 (예약 시 스킵)
  7 :결제 확인 단계 및 카드키 토출
  8 :영수증 출력 단계 (고객 선택 시) */

  let text = "판매대기";

  if (iscState.step === 1) {
    if (iscState.motion) text = "고객접근";
    else text = "고객대기";
  } else if (iscState.step === 2) {
    text = "성인인증 <i>모드</i>";
    if (!iscState.minor_mode) text += "성인인증 <i>완료</i>";
  } else if (iscState.step === 3) {
    text = "예약번호 입력";
    if (iscState.reserv_num) text = "예약번호<i>" + iscState.reserv_num + "</i>";
    if (iscState.reserv_error_msg) text += "예약오류<i>" + iscState.reserv_error_msg + "</i>";
  } else if (iscState.step === 4) {
    text = "객실선택";
    if (iscState.stay_type) text += "객실타입<i>" + iscState.stay_type + "</i>";
    if (iscState.room_id) text += "객실명<i>" + iscState.room_id + "</i>";
  } else if (iscState.step === 5) {
    text = "객실선택 확인";
    if (iscState.stay_type) text += "객실타입<i>" + iscState.stay_type + "</i>";
    if (iscState.room_id) text += "객실명<i>" + iscState.room_id + "</i>";
  } else if (iscState.step === 6) {
    text = "결제금액 " + iscState.pay_amt;

    // 현금 결제.
    if (iscState.act_input_unit) text += "현금결제<i>" + iscState.act_input_unit + "</i>";
    if (iscState.dsp_state) text += "거스름돈<i>" + iscState.dsp_change_unit + "</i>";
    if (iscState.dsp_not_change) text += "미방출<i>" + iscState.dsp_not_change + "</i>";

    // 카드 결제.
    if (iscState.crd_state) {
      text += "카드결제<i>" + iscState.crd_state + "</i>"; //  { value:0, text: 대기상태 , // 1 :결제중 // 2 :결제 완료 // 3 :결제 실패
      if (iscState.crd_card_no) text += "카드번호<i>" + iscState.crd_card_no + "</i>";
      if (iscState.crd_approval_no) text += "승인번호<i>" + iscState.crd_approval_no + "</i>";
    }
  } else if (iscState.step === 7) {
    text = "결제확인";
    text += "결제금액 " + iscState.pay_amt;

    // 현금 결제.
    if (iscState.act_input_unit) text += "현금결제<i>" + iscState.act_input_unit + "</i>";
    if (iscState.dsp_state) text += "거스름돈<i>" + iscState.dsp_change_unit + "</i>";
    if (iscState.dsp_not_change) text += "미방출<i>" + iscState.dsp_not_change + "</i>";

    // 카드 결제.
    if (iscState.crd_state) {
      text += "카드결제<i>" + iscState.crd_state + "</i>"; //  { value:0, text: 대기상태 , // 1 :결제중 // 2 :결제 완료 // 3 :결제 실패
      if (iscState.crd_card_no) text += "카드번호<i>" + iscState.crd_card_no + "</i>";
      if (iscState.crd_approval_no) text += "승인번호<i>" + iscState.crd_approval_no + "</i>";
    }

    // 카드키 배출.
    text += "카드키배출<i>" + iscState.kbx_dsp_error + "</i>"; // 0 :정상 , 1 :방출실패오류 , 2 :확인완료
  }

  return { text };
}

// 객실 상태 로그 데이터
export function roomDisplayStateLog(data, date) {
  let text = "";

  data = typeof data === "object" ? data : JSON.parse(data);

  _.map(data, (v, k) => {
    // console.log("- roomDisplayStateLog", k, v);
    if (v !== null && v !== undefined) {
      if (text) text += " , ";
      text += keyToValue("room_state_log", "key", k) + " [" + keyToValue("room_state", k, v) + "] ";
    }
  });

  if (date) text = "[" + moment(date).format("MM/DD HH:mm:ss") + "] " + text;

  // console.log("- roomDisplayStateLog text", text);

  return text;
}

// 자판기 관제 상태 로그 데이터
export function iscDisplayStateLog(data, date) {
  let text = "";

  data = typeof data === "object" ? data : JSON.parse(data);

  _.map(data, (v, k) => {
    // console.log("- iscDisplayStateLog", k, v);
    if (v !== null && v !== undefined) {
      if (text) text += " , ";
      text += keyToValue("isc_state", "key", k) + " [" + keyToValue("isc_state", k, v) + "] ";
    }
  });

  if (date) text = "[" + moment(date).format("MM/DD HH:mm:ss") + "] " + text;

  // console.log("- iscDisplayStateLog text", text);

  return text;
}
