/** 오류 정의 */
const ERROR = {
  DEFAULT: {
    code: 400,
    message: "요청 처리중 오류가 발생 했습니다.",
    detail: "관리자에게 문의해 주세요.",
  },
  REQUIRE_ARGUMENT: {
    code: 400,
    message: "필수 입력 정보가 없습니다.",
    detail: "입력 정보 확인 후 다시 시도해 주세요.",
  },
  INVALID_ARGUMENT: {
    code: 400,
    message: "요청 정보가 올바르지 않습니다.",
    detail: "요청 정보 확인 후 다시 시도해 주세요.",
  },
  NO_DATA: {
    code: 400,
    message: "요청 정보가 존재 하지 않습니다.",
    detail: "요청 정보 확인 후 다시 시도해 주세요.",
  },
  INVALID_USER: {
    code: 401,
    message: "유효한 사용자가 아닙니다.",
    detail: "관리자에게 문의해 주세요.",
  },
  NO_CERTIFICATION: {
    code: 401,
    message: "인증 정보가 없습니다.",
    detail: "인증 후 다시 시도해 주세요.",
  },
  INVALID_CERTIFICATION: {
    code: 401,
    message: "인증 정보가 유효하지 않습니다.",
    detail: "관리자에게 문의해 주세요.",
  },
  EXPIRE_CERTIFICATION: {
    code: 401,
    message: "인증 기간이 만료 되었습니다.",
    detail: "관리자에게 문의해 주세요.",
  },
  INVALID_AUTHORITY: {
    code: 401,
    message: "접근 권한이 없습니다.",
    detail: "로그인 후 다시 시도해 주세요.",
  },
  FAIL_QR_CODE: {
    code: 401,
    message: "QR 코드 생성이 실패 했습니다.",
    detail: "관리자에게 문의해 주세요.",
  },
};

/** 메세지 정의 */
const MESSAGE = {
  DEFAULT: {
    title: "알림",
    message: "메세지를 입력해 주세요.",
  },
  LOGIN: {
    title: "로그인",
    message: "정상적으로 로그인 되었습니다.",
  },
  LOGOUT: {
    title: "로그아웃",
    message: "정상적으로 로그아웃 되었습니다.",
  },
  CONFIRM_REGIST: {
    title: "등록",
    message: "등록 하시겠습니까?",
  },
  CONFIRM_MODIFY: {
    title: "수정",
    message: "수정 하시겠습니까?",
  },
  CONFIRM_DELETE: {
    title: "삭제",
    message: "삭제 하시겠습니까?",
  },
  ALERT_INFO: {
    title: "정보",
    message: "정보 알림 입니다.",
  },
  ALERT_WARN: {
    title: "경고",
    message: "경고 알림 입니다.",
  },
  ALERT_ERROR: {
    title: "오류",
    message: "오류 알림 입니다.",
  },
};

const PAGE_ROW_CNT = 10; // 한 페이지당 row 수
const PAGE_GROUP_CNT = 10; // 페이징 버튼 갯수

const getSessionStroge = (name, init) => {
  let value = sessionStorage.getItem(name) || "";
  if (init) setSessionStroge(name, init);
  return isNaN(value) ? value : Number(value);
};

const setSessionStroge = (name, value) => {
  sessionStorage.setItem(name, String(value));
};

const getLocalStroge = (name, init) => {
  let value = localStorage.getItem(name) || "";
  if (init) setLocalStroge(name, init);
  return isNaN(value) ? value : Number(value);
};

const setLocalStroge = (name, value) => {
  localStorage.setItem(name, String(value));
};

export { ERROR, MESSAGE, PAGE_ROW_CNT, PAGE_GROUP_CNT, getSessionStroge, setSessionStroge, getLocalStroge, setLocalStroge };
