import Router from "vue-router";
import routes from "./routes";
import $ from "jquery";

// configure router
const router = new Router({
  mode: "history",
  routes, // short for routes: routes
  linkExactActiveClass: "active",
  scrollBehavior: (to) => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  },
});

router.afterEach((to, from, next) => {
  // console.log("- router afterEach", { to, from });

  setTimeout(() => {
    // 마지막 포커싱 객체 저장.
    $("input")
      .off("focus")
      .on("focus", function() {
        // console.log("- focusin");
        window.lastFocused = $(this);
      });
  }, 100);
});

export default router;
