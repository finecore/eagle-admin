import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Admin pages
import Login from "@/pages/Login.vue";
import Dashboard from "@/pages/Dashboard.vue";
import Company from "@/pages/Company.vue";
import Place from "@/pages/Place.vue";
import Device from "@/pages/Device.vue";
import Sales from "@/pages/Sales.vue";
import User from "@/pages/User.vue";
import Profile from "@/pages/Profile.vue";
import Notifications from "@/pages/Notifications.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Typography from "@/pages/Typography.vue";
import TableList from "@/pages/TableList.vue";
import Center from "@/pages/Center.vue";
import Version from "@/pages/Version.vue";
import Subscribe from "@/pages/Subscribe.vue";
import Notice from "@/pages/Notice.vue";
import As from "@/pages/As.vue";
import MmsMo from "@/pages/MmsMo.vue";
import MailReceiver from "@/pages/MailReceiver.vue";

const routes = [
  {
    path: "/login", // 첫 화면을 로그인 화면으로 설정한다
    name: "login",
    component: Login,
  },
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    props: true,
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard,
        props: true,
      },
      {
        path: "center",
        name: "center",
        component: Center,
        props: true,
      },
      {
        path: "company",
        name: "company",
        component: Company,
        props: true,
      },
      {
        path: "place",
        name: "place",
        component: Place,
        props: true,
      },
      {
        path: "device",
        name: "device",
        component: Device,
        props: true,
      },
      {
        path: "sales",
        name: "devisalesce",
        component: Sales,
        props: true,
      },
      {
        path: "notice",
        name: "notice",
        component: Notice,
        props: true,
      },
      {
        path: "as",
        name: "as",
        component: As,
        props: true,
      },
      {
        path: "mms-mo",
        name: "mmsMo",
        component: MmsMo,
        props: true,
      },
      {
        path: "user",
        name: "user",
        component: User,
        props: true,
      },
      {
        path: "subscribe",
        name: "subscribe",
        component: Subscribe,
        props: true,
      },
      {
        path: "mail-receiver",
        name: "mailReceiver",
        component: MailReceiver,
        props: true,
      },
      {
        path: "version",
        name: "version",
        component: Version,
        props: true,
      },
      {
        path: "profile",
        name: "profile",
        component: Profile,
        props: true,
      },
      {
        path: "maps",
        name: "maps",
        component: Maps,
        props: true,
      },
      {
        path: "notifications",
        name: "notifications",
        component: Notifications,
        props: true,
      },
      {
        path: "icons",
        name: "icons",
        component: Icons,
        props: true,
      },
      {
        path: "typography",
        name: "typography",
        component: Typography,
        props: true,
      },
    ],
  },
  { path: "*", component: NotFound },
];

export default routes;
