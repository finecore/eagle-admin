import { Doughnut, mixins } from "vue-chartjs";

export default {
  name: "doughnut-chart",
  extends: Doughnut,
  mixins: [mixins.reactiveProp],
  props: {
    chartData: { type: Array | Object, required: false },
    extraOptions: Object,
    gradientColors: {
      type: Array,
      default: () => ["rgba(58,161,234,1)", "rgba(27,120,200,1)", "rgba(12,99,183,1)"],
      validator: val => {
        return val.length > 2;
      }
    },
    gradientStops: {
      type: Array,
      default: () => [1, 0.6, 0.3],
      validator: val => {
        return val.length > 2;
      }
    }
  },
  data() {
    return {
      ctx: null
    };
  },
  methods: {
    updateGradients(chartData) {
      if (!chartData) return;
      const ctx = this.ctx || document.getElementById(this.chartId).getContext("2d");
      const gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

      gradientStroke.addColorStop(this.gradientStops[0], this.gradientColors[0]);
      gradientStroke.addColorStop(this.gradientStops[1], this.gradientColors[1]);
      gradientStroke.addColorStop(this.gradientStops[2], this.gradientColors[2]);
      chartData.datasets.forEach(set => {
        set.backgroundColor = [gradientStroke, "#2c323a"];
      });
    }
  },
  mounted() {
    this.$watch(
      "chartData",
      (newVal, oldVal) => {
        this.updateGradients(newVal);
        if (!oldVal) {
          this.renderChart(newVal, this.extraOptions);
        }
      },
      { immediate: true, responsive: true, maintainAspectRatio: false }
    );
  }
};
