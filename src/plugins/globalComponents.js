import { BaseInput, Card, BaseDropdown, BaseButton, BaseCheckbox, BaseLoader } from "../components/index";

import Paginate from "vuejs-paginate";
import JsonExcel from "vue-json-excel";

/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */
const GlobalComponents = {
  install(Vue) {
    Vue.component(BaseInput.name, BaseInput);
    Vue.component(Card.name, Card);
    Vue.component(BaseDropdown.name, BaseDropdown);
    Vue.component(BaseButton.name, BaseButton);
    Vue.component(BaseCheckbox.name, BaseCheckbox);
    Vue.component(BaseLoader.name, BaseLoader);
    Vue.component("paginate", Paginate);
    Vue.component("downloadExcel", JsonExcel);
  },
};

export default GlobalComponents;
