export default {
  bind: function(el, binding, vnode) {
    el.keyUpToUpperCase = function(e) {
      e.target.value = e.target.value.toUpperCase();
      vnode.componentInstance.$emit("input", e.target.value.toUpperCase());
    };
    document.body.addEventListener("keyup", el.keyUpToUpperCase);
  },
  unbind: function(el) {
    document.body.removeEventListener("keyup", el.keyUpToUpperCase);
  }
};
