const debug = process.env.NODE_ENV !== "production";

const HOST = process.env.VUE_APP_API_SERVER_HOST;
const PORT = process.env.VUE_APP_API_SERVER_PORT;
const HOST_URL = HOST + ":" + PORT;

console.log("-> API Server ", HOST_URL, " debug:", debug);

export { HOST, PORT, HOST_URL };
