import { HOST_URL } from "./config";

import { api, ajax } from "@/utils/api-util";

// list
const list = (dispatch, url, data) => {
  return ajax
    .get(`${HOST_URL}/${url}`, { ...data, dispatch })
    .then((res) => api.checkResponse(dispatch, res))
    .catch((error) => api.errorRequest(dispatch, error));
};

// item
const item = (dispatch, url, data) => {
  return ajax
    .get(`${HOST_URL}/${url}`, { ...data, dispatch })
    .then((res) => api.checkResponse(dispatch, res))
    .catch((error) => api.errorRequest(dispatch, error));
};

// post
const post = (dispatch, url, data) => {
  return ajax
    .post(`${HOST_URL}/${url}`, data, { dispatch })
    .then((res) => api.checkResponse(dispatch, res))
    .catch((error) => api.errorRequest(dispatch, error));
};

// put
const put = (dispatch, url, data) => {
  return ajax
    .put(`${HOST_URL}/${url}`, data, { dispatch })
    .then((res) => api.checkResponse(dispatch, res))
    .catch((error) => api.errorRequest(dispatch, error));
};

// del
const del = (dispatch, url, data) => {
  return ajax
    .delete(`${HOST_URL}/${url}`, data, { dispatch })
    .then((res) => api.checkResponse(dispatch, res))
    .catch((error) => api.errorRequest(dispatch, error));
};

// login
const login = (dispatch, data) => {
  return ajax
    .post(`${HOST_URL}/login`, data, { dispatch })
    .then((res) => api.checkResponse(dispatch, res))
    .catch((error) => api.errorRequest(dispatch, error));
};

export { list, item, post, put, del, login };
