import Vue from "vue";
import Vuex from "vuex";

import createLogger from "vuex/dist/logger";
// import createPersistedState from "vuex-persistedstate"; // 화면 갱신시 state 유지.

import WebSocketUtil from "../utils/websocket-util";

// modules
import dashboard from "./modules/dashboard";
import ws from "./modules/ws";
import error from "./modules/error";
import com from "./modules/com";
import auth from "./modules/auth";
import user from "./modules/user";
import preferences from "./modules/preferences";
import company from "./modules/company";
import place from "./modules/place";
import device from "./modules/device";
import roomSaleReport from "./modules/room-sale-report";
import userPlace from "./modules/user-place";
import iscSet from "./modules/isc-set";
import iscState from "./modules/isc-state";
import iscStateLog from "./modules/isc-state-log";
import iscKeyBox from "./modules/isc-key-box";
import version from "./modules/version";
import file from "./modules/file";
import subscribe from "./modules/subscribe";
import placeSubscribe from "./modules/place-subscribe";
import notice from "./modules/notice";
import as from "./modules/as";
import room from "./modules/room";
import roomState from "./modules/room-state";
import roomStateLog from "./modules/room-state-log";
import roomType from "./modules/room-type";
import roomReserv from "./modules/room-reserv";
import mmsMo from "./modules/mms-mo";
import mailReceiver from "./modules/mail-receiver";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

const plugins = []; // [createPersistedState()];

if (debug) plugins.push(createLogger());

const store = new Vuex.Store({
  modules: {
    dashboard,
    ws,
    error,
    com,
    auth,
    user,
    version,
    file,
    preferences,
    company,
    place,
    device,
    roomSaleReport,
    userPlace,
    iscSet,
    iscState,
    iscStateLog,
    iscKeyBox,
    subscribe,
    placeSubscribe,
    notice,
    as,
    room,
    roomState,
    roomStateLog,
    roomType,
    roomReserv,
    mmsMo,
    mailReceiver,
  },
  strict: debug,
  plugins: plugins,
});

// 웹소켓 Store 설정.
const webSocket = new WebSocketUtil(store);

webSocket.connect();

export default store;
