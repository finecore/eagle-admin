import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state[COUNT];
    },
    [LIST](state) {
      return state[LIST];
    },
    [ITEM](state) {
      return state[ITEM];
    },
  },
  mutations: {
    [LIST](state, { count, room_states, page }) {
      state.count = count;
      state.list = room_states;
      if (page) state.page = page;
    },
    [ITEM](state, { room_state }) {
      state.item = room_state[0] || room_state;

      mergeList(state.list, state.item, "room_id");
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page, filter = "1=1", order = "a.reg_date", desc = "desc", limit = 10000 }) {
      if (page) {
        const { no = 1, size = getSessionStroge("rowSize") } = page;
        limit = (no - 1) * size + "," + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(dispatch, `room/state/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, room_states } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            room_states,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return room_states;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, id) {
      const promise = await item(dispatch, `room/state/${id}`).then(({ common: { success }, body: { room_state } }) => {
        if (success) {
          commit(ITEM, { room_state });
        } else {
          commit(ITEM, []);
        }
        return room_state;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, room_state) {
      const promise = await post(dispatch, `room/state`, { room_state }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let room_states = state.list.concat(room_state);
          commit(LIST, { count: state.count++, room_states });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, room_state) {
      const promise = await put(dispatch, `room/state/${room_state.id}`, {
        room_state,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          commit(ITEM, { room_state });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `room/state/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let room_states = state.list.filter((item) => item.id !== id);
          commit(LIST, { count: state.count--, room_states });
        }
        return success;
      });
      return promise;
    },
  },
};
