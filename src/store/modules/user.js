import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, SELECTED, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    count: 0,
    list: [],
    item: {},
    page: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state[COUNT];
    },
    [LIST](state) {
      return state[LIST];
    },
    [ITEM](state) {
      return state[ITEM];
    },
  },
  mutations: {
    [LIST](state, { count, users, page }) {
      state.count = count;
      state[LIST] = users;
      if (page) state.page = page;
    },
    [ITEM](state, { user }) {
      state[ITEM] = user;
      mergeList(state[LIST], state[ITEM], "id");
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page, filter = "1=1", order = "id", desc = "desc", limit = 10000 }) {
      if (page) {
        const { no = 1, size = getSessionStroge("rowSize") } = page;
        limit = (no - 1) * size + "," + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(dispatch, `user/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, users } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            users,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return users;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, id) {
      const promise = await item(dispatch, `user/${id}`).then(({ common: { success }, body: { user } }) => {
        if (success) {
          commit(ITEM, { user });
        } else {
          commit(ITEM, []);
        }
        return user;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, user) {
      const promise = await post(dispatch, `user`, { user }).then(({ common: { success }, body: { info, user } }) => {
        if (success) {
          dispatch("getList", { page: 1 });
        }
        return { success, user };
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, user) {
      const promise = await put(dispatch, `user/${user.id}`, { user }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          commit(ITEM, { user });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `user/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          dispatch("getList", { page: 1 });
        }
        return success;
      });
      return promise;
    },
  },
};
