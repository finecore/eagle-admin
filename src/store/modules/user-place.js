import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, SELECTED, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
    [SELECTED]: 0,
  },
  getters: {
    [COUNT](state) {
      return state[COUNT];
    },
    [LIST](state) {
      return state[LIST];
    },
    [ITEM](state) {
      return state[ITEM];
    },
    [SELECTED](state) {
      return state[SELECTED];
    },
  },
  mutations: {
    [LIST](state, { count, user_places, page }) {
      state[COUNT] = count;
      state[LIST] = user_places;
      if (page) state[PAGE] = page;
    },
    [ITEM](state, { user_place }) {
      state[ITEM] = user_place[0] || user_place;
      mergeList(state[LIST], state[ITEM], "id");
    },
    [SELECTED](state, id) {
      state[SELECTED] = id;
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page, filter = "1=1", order = "reg_date", desc = "desc", limit = 10000 }) {
      if (page) {
        const { no = 1, size = getSessionStroge("rowSize") } = page;
        limit = (no - 1) * size + "," + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(dispatch, `user/place/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, user_places } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            user_places,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, { user_places: [] });
        }
        return user_places;
      });
      return promise;
    },
    async getListTemp({ state, commit, dispatch }, { filter = "1=1", order = "reg_date", desc = "desc", limit = 10000 }) {
      const promise = await list(dispatch, `user/place/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, user_places } }) => {
        return { success, count, user_places };
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, id) {
      const promise = await item(dispatch, `user/place/${id}`).then(({ common: { success }, body: { user_place } }) => {
        if (success) {
          commit(ITEM, { user_place });
        } else {
          commit(ITEM, { user_place: {} });
        }
        return user_place;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, user_place) {
      const promise = await post(dispatch, `user/place`, { user_place }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          dispatch("getList", { page: 1 });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, user_place) {
      const promise = await put(dispatch, `user/place/${user_place.id}`, {
        user_place,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          commit(ITEM, { user_place });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `user/place/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          dispatch("getList", { page: 1 });
        }
        return success;
      });
      return promise;
    },
  },
};
