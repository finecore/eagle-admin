import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state.count;
    },
    [LIST](state) {
      return state.list;
    },
    [ITEM](state) {
      return state.item;
    },
  },
  mutations: {
    [LIST](state, { count, isc_sets, page }) {
      state.count = count;
      state.list = isc_sets;
      if (page) state.page = page;
    },
    [ITEM](state, { isc_set }) {
      state.item = isc_set[0] || isc_set;
      mergeList(state.list, state.item, "id");
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page, filter = "1=1", order = "reg_date", desc = "desc", limit = 10000 }) {
      if (page) {
        const { no = 1, size = getSessionStroge("rowSize") } = page;
        limit = (no - 1) * size + "," + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(dispatch, `isc/set/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, isc_sets } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            isc_sets,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return isc_sets;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, serialno) {
      const promise = await item(dispatch, `isc/set/${serialno}`).then(({ common: { success }, body: { isc_set } }) => {
        if (success) {
          commit(ITEM, { isc_set });
        } else {
          commit(ITEM, []);
        }
        return isc_set;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, isc_set) {
      const promise = await post(dispatch, `isc/set`, { isc_set }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          isc_set.id = info.insertId;
          let isc_sets = state.list.concat(isc_set);
          commit(LIST, { count: state.count++, isc_sets });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, isc_set) {
      const promise = await put(dispatch, `isc/set/${isc_set.id}`, {
        isc_set,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let isc_sets = state.list.map((item) => (item.id !== isc_set.id ? item : isc_set));
          commit(LIST, { count: state.count, isc_sets });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `isc/set/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let isc_sets = state.list.filter((item) => item.id !== id);
          commit(LIST, { count: state.count--, isc_sets });
        }
        return success;
      });
      return promise;
    },
  },
};
