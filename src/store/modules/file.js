import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state.count;
    },
    [LIST](state) {
      return state.list;
    },
    [ITEM](state) {
      return state.item;
    },
  },
  mutations: {
    [LIST](state, { count, files, page }) {
      state.count = count;
      state.list = files;
      if (page) state.page = page;
    },
    [ITEM](state, { file }) {
      state.item = file[0] || file;
      mergeList(state.list, state.item, "id");
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page, filter = "1=1", order = "reg_date", desc = "desc", limit = 10000 }) {
      if (page) {
        const { no = 1, size = getSessionStroge("rowSize") } = page;
        limit = (no - 1) * size + "," + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      console.log("- file getList", filter);

      const promise = await list(dispatch, `file/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, files } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            files,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return files;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, id) {
      const promise = await item(dispatch, `file/${id}`).then(({ common: { success }, body: { file } }) => {
        if (success) {
          commit(ITEM, { file });
        } else {
          commit(ITEM, []);
        }
        return file;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, file) {
      const promise = await post(dispatch, `file`, { file }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          file.id = info.insertId;
          let files = state.list ? state.list.concat(file) : [file];
          //  commit(LIST, { count: state.count + 1, files });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, file) {
      const promise = await put(dispatch, `file/${file.id}`, { file }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let files = state.list.map((item) => (item.id !== file.id ? item : file));
          //   commit(LIST, { count: state.count, files });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `file/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let files = state.list.filter((item) => item.id !== id);
          //   commit(LIST, { count: state.count - 1, files });
        }
        return success;
      });
      return promise;
    },
  },
};
