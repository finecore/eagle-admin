import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state.count;
    },
    [LIST](state) {
      return state.list;
    },
    [ITEM](state) {
      return state.item;
    },
  },
  mutations: {
    [LIST](state, { count, place_subscribes, page }) {
      state.count = count;
      state.list = place_subscribes;
      if (page) state.page = page;
    },
    [ITEM](state, { place_subscribe }) {
      state.item = place_subscribe[0] || place_subscribe;
      mergeList(state.list, state.item, "id");
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page = 0, filter = "1=1", order = "a.reg_date", desc = "desc", limit = 10000 }) {
      const promise = await list(dispatch, `place/subscribe/list/${filter}/${order}/${desc}/${limit}`, {
        page,
      }).then(({ common: { success }, body: { count, place_subscribes } }) => {
        if (success) {
          commit(LIST, { count: count[0].count, place_subscribes, page: _.cloneDeep(page) }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return { success, place_subscribes };
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, id) {
      const promise = await item(dispatch, `place/subscribe/${id}`).then(({ common: { success }, body: { place_subscribe } }) => {
        if (success) {
          commit(ITEM, { place_subscribe });
        } else {
          commit(ITEM, []);
        }
        return place_subscribe;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, place_subscribe) {
      const promise = await post(dispatch, `place/subscribe`, { place_subscribe }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          place_subscribe.id = info.insertId;
          commit(ITEM, { place_subscribe });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, place_subscribe) {
      const promise = await put(dispatch, `place/subscribe/${place_subscribe.id}`, {
        place_subscribe,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          commit(ITEM, { place_subscribe });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `place/subscribe/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let place_subscribes = state.list.filter((item) => item.id !== id);
          commit(LIST, { count: state.count - 1, place_subscribes });
        }
        return success;
      });
      return promise;
    },
  },
};
