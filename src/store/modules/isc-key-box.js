import { list, item, post, put, del } from "@/api";
import { LIST, USE_LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [USE_LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state[COUNT];
    },
    [LIST](state) {
      return state[LIST];
    },
    [USE_LIST](state) {
      return state[USE_LIST];
    },
    [ITEM](state) {
      return state[ITEM];
    },
  },
  mutations: {
    [LIST](state, { count, isc_key_boxs, page }) {
      state[COUNT] = count;
      state[LIST] = isc_key_boxs;
      if (page) state[PAGE] = page;
    },
    [USE_LIST](state, { isc_key_boxs }) {
      state[USE_LIST] = isc_key_boxs;
    },
    [ITEM](state, { isc_key_box }) {
      state[ITEM] = isc_key_box[0] || isc_key_box;

      mergeList(state.list, state.item, "id");
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page, filter = "1=1", order = "gid,did", desc = "asc", limit = 10000 }) {
      if (page) {
        const { no = 1, size = getSessionStroge("rowSize") } = page;
        limit = (no - 1) * size + "," + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(dispatch, `isc/key/box/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, isc_key_boxs } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            isc_key_boxs,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return isc_key_boxs;
      });
      return promise;
    },
    async getUseList({ state, commit, dispatch }) {
      const promise = await list(dispatch, `isc/key/box/use/list`).then(({ common: { success }, body: { isc_key_boxs } }) => {
        if (success) {
          commit(USE_LIST, {
            isc_key_boxs,
          });
        } else {
          commit(USE_LIST, []);
        }
        return isc_key_boxs;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, serialno) {
      const promise = await item(dispatch, `isc/key/box/${serialno}`).then(({ common: { success }, body: { isc_key_box } }) => {
        if (success) {
          commit(ITEM, { isc_key_box });
        } else {
          commit(ITEM, []);
        }
        return isc_key_box;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, isc_key_box) {
      const promise = await post(dispatch, `isc/key/box`, { isc_key_box }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let isc_key_boxs = state.list.concat(isc_key_box);
          commit(LIST, { count: state.count++, isc_key_boxs });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, isc_key_box) {
      const promise = await put(dispatch, `isc/key/box/${isc_key_box.id}`, {
        isc_key_box,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          commit(ITEM, { isc_key_box });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `isc/key/box/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let isc_key_boxs = state.list.filter((item) => item.id !== id);
          commit(LIST, { count: state.count--, isc_key_boxs });
        }
        return success;
      });
      return promise;
    },
  },
};
