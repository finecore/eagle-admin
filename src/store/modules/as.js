import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import moment from "moment";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state.count;
    },
    [LIST](state) {
      return state.list;
    },
    [ITEM](state) {
      return state.item;
    },
  },
  mutations: {
    [LIST](state, { count, ases, page }) {
      state.count = count;
      state.list = ases;
      if (page) state.page = page;
    },
    [ITEM](state, { as }) {
      state.item = as[0] || as;
      mergeList(state.list, state.item, "id");
    },
  },
  actions: {
    async getList(
      { state, commit, dispatch },
      {
        page,
        filter = "1=1",
        between = "a.reg_date",
        begin = moment()
          .add(-3, "year")
          .format("YYYY-MM-DD"),
        end = moment()
          .add(1, "day")
          .format("YYYY-MM-DD"),
      }
    ) {
      const { no = 1, size = getSessionStroge("rowSize") } = page;
      let limit = (no - 1) * size + "," + size;

      const promise = await list(dispatch, `as/list/${filter}/${between}/${begin}/${end}/${limit}`).then(({ common: { success }, body: { count, ases } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            ases,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return ases;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, serialno) {
      const promise = await item(dispatch, `as/${serialno}`).then(({ common: { success }, body: { as } }) => {
        if (success) {
          commit(ITEM, { as });
        } else {
          commit(ITEM, []);
        }
        return as;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, as) {
      const promise = await post(dispatch, `as`, { as }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          as.id = info.insertId;
          commit(ITEM, { as });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, as) {
      const promise = await put(dispatch, `as/${as.id}`, {
        as,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          commit(ITEM, { as });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `as/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let ass = state.list.filter((item) => item.id !== id);
          commit(LIST, { count: state.count - 1, ass });
        }
        return success;
      });
      return promise;
    },
  },
};
