// import { AUTH, IS_AUTH } from "@/store/mutation_types";
// import { post } from "@/api";
// import router from "@/router/index";

const JOIN_REQUESTED = "JOIN_REQUESTED";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    uuid: "",
  },
  getters: {
    uuid(state) {
      return state.uuid;
    },
  },
  mutations: {
    uuid(state, { uuid }) {
      state.uuid = uuid;
      sessionStorage.setItem("uuid", uuid);
    },
  },
  actions: {
    async join({ state, commit, dispatch }, { webSocket, user }) {
      if (webSocket) {
        console.log("----> ws join  ", user);
        webSocket.sendMessage(JOIN_REQUESTED, { channel: "admin", user });
      }
    },
    async setUuid({ state, commit, dispatch }, { uuid }) {
      this.commit("uuid", { uuid });
    },
  },
};
