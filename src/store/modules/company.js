import { LIST, ITEM, COUNT } from "@/store/mutation_types";
import { list, item, post, put, del } from "@/api";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    count: 0,
    list: [],
    item: {},
    page: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state[COUNT];
    },
    [LIST](state) {
      return state[LIST];
    },
    [ITEM](state) {
      return state[ITEM];
    },
  },
  mutations: {
    [LIST](state, { count, list, page }) {
      state.count = count;
      state.list = list;
      if (page) state.page = page;
    },
    [ITEM](state, item) {
      state.item = item;
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page, filter = "1=1", order = "reg_date", desc = "desc", limit = 10000 }) {
      if (page) {
        const { no = 1, size = getSessionStroge("rowSize") } = page;
        limit = (no - 1) * size + "," + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(dispatch, `company/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, companys: list } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            list,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return list;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, id) {
      const promise = await item(dispatch, `company/${id}`).then(({ common: { success }, body: { company: item } }) => {
        if (success) {
          commit(ITEM, item);
        } else {
          commit(ITEM, []);
        }
        return item;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, company) {
      const promise = await post(dispatch, `company`, { company }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          dispatch("getList", { page: state.page });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, company) {
      const promise = await put(dispatch, `company/${company.id}`, {
        company,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let list = state.list.map((item) => (item.id !== company.id ? item : company));

          commit(LIST, { count: state.count, list });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `company/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          dispatch("getList", { page: state.page });
        }
        return success;
      });
      return promise;
    },
  },
};
