import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import moment from "moment";
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {},
  getters: {},
  mutations: {},
  actions: {
    async totalPlaceSido({ state, commit, dispatch }, args) {
      const promise = await list(dispatch, `dashboard/total/place/sido`, { ...args }).then(({ common: { success }, body: { count, result } }) => {
        return result;
      });
      return promise;
    },
    async totalNowIeg({ state, commit, dispatch }, args) {
      const promise = await list(dispatch, `dashboard/total/now/ieg`, { ...args }).then(({ common: { success }, body: { count, result } }) => {
        return result;
      });
      return promise;
    },
    async listNowIeg({ state, commit, dispatch }, args) {
      let { filter = "1=1", order = "reg_date", desc = "desc", limit = 10000, loading = true } = args;

      const promise = await list(dispatch, `dashboard/list/now/ieg/${filter}/${order}/${desc}/${limit}`, { loading }).then(({ common: { success }, body: { count, result } }) => {
        return result;
      });
      return promise;
    },
    async totalNowIsg({ state, commit, dispatch }, args) {
      const promise = await list(dispatch, `dashboard/total/now/isg`, { ...args }).then(({ common: { success }, body: { count, result } }) => {
        return result;
      });
      return promise;
    },
    async listNowIsg({ state, commit, dispatch }, args) {
      let { filter = "1=1", order = "reg_date", desc = "desc", limit = 10000, loading = true } = args;

      const promise = await list(dispatch, `dashboard/list/now/isg/${filter}/${order}/${desc}/${limit}`, { loading }).then(({ common: { success }, body: { count, result } }) => {
        return result;
      });
      return promise;
    },
    async totalReservSum({ state, commit, dispatch }, args) {
      let { filter = "1=1", begin = moment().format("YYYY-MM-DD"), end = moment().format("YYYY-MM-DD"), type = "day", groups = "stay_type", loading = true } = args;

      const promise = await list(dispatch, `dashboard/total/reserv/sum/${filter}/${begin}/${end}/${type}/${groups}`, { loading }).then(({ common: { success }, body: { count, result } }) => {
        return result;
      });
      return promise;
    },
    async totalRoomStatusSum({ state, commit, dispatch }, args) {
      let { filter = "1=1", groups = "signal", loading = true } = args;

      const promise = await list(dispatch, `dashboard/total/room/status/sum/${filter}/${groups}`, { loading }).then(({ common: { success }, body: { count, result } }) => {
        return result;
      });
      return promise;
    },
    async totalIsgStatusSum({ state, commit, dispatch }, args) {
      let { filter = "1=1", groups = "state", loading = true } = args;

      const promise = await list(dispatch, `dashboard/total/isg/status/sum/${filter}/${groups}`, { loading }).then(({ common: { success }, body: { count, result } }) => {
        return result;
      });
      return promise;
    },
    async nowEventList({ state, commit, dispatch }, args) {
      let { filter = "1=1", limit = 100, loading = true } = args;

      const promise = await list(dispatch, `dashboard/list/now/event/${filter}/${limit}`, { loading }).then(({ common: { success }, body: { count, result } }) => {
        return result;
      });
      return promise;
    },
    async totalReportIeg({ state, commit, dispatch }, args) {
      let { filter = "1=1", begin = moment().format("YYYY-MM-DD"), end = moment().format("YYYY-MM-DD"), type = "month", groups = "a.reg_date", loading = true } = args;

      const promise = await list(dispatch, `dashboard/total/report/ieg/${filter}/${begin}/${end}/${type}/${groups}`, { ...args }).then(({ common: { success }, body: { count, result } }) => {
        return result;
      });
      return promise;
    },
    async totalReportIsg({ state, commit, dispatch }, args) {
      let { filter = "1=1", begin = moment().format("YYYY-MM-DD"), end = moment().format("YYYY-MM-DD"), type = "month", groups = "a.reg_date", loading = true } = args;

      const promise = await list(dispatch, `dashboard/total/report/isg/${filter}/${begin}/${end}/${type}/${groups}`, { ...args }).then(({ common: { success }, body: { count, result } }) => {
        return result;
      });
      return promise;
    },
  },
};
