import { AUTH, IS_AUTH, TOKEN, DUP_ACCESS_LOGOUT, DUP_ACCESS_FAILUE, IS_SUPERVISOR, IS_ADMIN, IS_VIEWER } from "@/store/mutation_types";
import { post } from "@/api";
import router from "@/router/index";

function ParseJsonString(str) {
  try {
    return JSON.parse(str);
  } catch (e) {}
  return null;
}

export default {
  state: {
    // 화면 reload 시 로그인 정보 재설정 한다.(로그인 정보만 재설정)
    [AUTH]: ParseJsonString(sessionStorage.getItem("user")),
    [TOKEN]: sessionStorage.getItem("token"),
    [IS_AUTH]: sessionStorage.getItem("token") !== null,
    [DUP_ACCESS_LOGOUT]: false,
    [DUP_ACCESS_FAILUE]: false,
  },
  getters: {
    [AUTH](state) {
      return state[AUTH];
    },
    [TOKEN](state) {
      return state[TOKEN];
    },
    [IS_AUTH](state) {
      return state[IS_AUTH];
    },
    [IS_SUPERVISOR](state) {
      if (state[IS_AUTH]) {
        const { type, level } = state[AUTH];
        return type === 1 && level === 0;
      }
      return false;
    },
    [IS_ADMIN](state) {
      if (state[IS_AUTH]) {
        const { type, level } = state[AUTH];
        return type === 1 && level < 3;
      }
      return false;
    },
    [IS_VIEWER](state) {
      if (state[IS_AUTH]) {
        const { type, level } = state[AUTH];
        return type === 1 && level === 9;
      }
      return false;
    },
    [DUP_ACCESS_LOGOUT](state) {
      return state[DUP_ACCESS_LOGOUT];
    },
    [DUP_ACCESS_FAILUE](state) {
      return state[DUP_ACCESS_FAILUE];
    },
  },
  mutations: {
    [AUTH](state, { user, token }) {
      state[AUTH] = user;
      state[IS_AUTH] = true;
      if (token) state.toke = token;
    },
    [IS_AUTH](state, isAuth) {
      state[IS_AUTH] = isAuth;
    },
    // 현재 사용 안함 추후 동시 접속 라이선스 기능 사용 시 적용.
    [DUP_ACCESS_LOGOUT](state, isAuth) {
      state[DUP_ACCESS_LOGOUT] = isAuth;
    },
    // 현재 사용 안함 추후 동시 접속 라이선스 기능 사용 시 적용.
    [DUP_ACCESS_FAILUE](state, isAuth) {
      state[DUP_ACCESS_FAILUE] = isAuth;
    },
  },
  actions: {
    async login({ state, commit, dispatch }, { id, pwd }) {
      // call api.
      const promise = await post(dispatch, "login", { id, pwd }).then(({ common: { success }, body: { user, token } }) => {
        if (success) {
          commit(AUTH, { user, token });
        } else {
          commit(AUTH, { user: null, token: null });
        }
        commit(IS_AUTH, success);

        sessionStorage.setItem("user", JSON.stringify(user)); // user 정보 저장.
        sessionStorage.setItem("token", token); // token 정보 저장.
        localStorage.setItem("loginId", id); // 최종 로그인 id 정보 로컬 저장.

        return { success, user };
      });
      return promise;
    },

    async logout({ state, commit, dispatch }) {
      sessionStorage.removeItem("user"); // user 정보 삭제.
      sessionStorage.removeItem("token"); // token 정보 저장.

      commit(AUTH, { user: null, token: null });
      commit(IS_AUTH, false);

      router.push("login");

      return true;
    },
  },
};
