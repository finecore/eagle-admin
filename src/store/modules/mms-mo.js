import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import moment from "moment";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state.count;
    },
    [LIST](state) {
      return state.list;
    },
    [ITEM](state) {
      return state.item;
    },
  },
  mutations: {
    [LIST](state, { count, mms_mos }) {
      state.count = count;
      state.list = mms_mos;
    },
    [ITEM](state, { mms_mo }) {
      state.item = mms_mo[0] || mms_mo;
      mergeList(state.list, state.item, "MO_KEY");
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { no = 1, size = getSessionStroge("rowSize"), filter = "1=1", order = "a.MO_KEY", desc = "desc" }) {
      let limit = (no - 1) * size + "," + size;

      const promise = await list(dispatch, `mms/mo/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, mms_mos } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            mms_mos,
          });
        }
        return mms_mos;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, id) {
      const promise = await item(dispatch, `mms/mo/${id}`).then(({ common: { success }, body: { mms_mo } }) => {
        if (success) {
          commit(ITEM, { mms_mo });
        }
        return mms_mo;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, mms_mo) {
      const promise = await post(dispatch, `mms/mo`, { mms_mo }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          mms_mo.id = info.insertId;
          commit(ITEM, { mms_mo });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, mms_mo) {
      const promise = await put(dispatch, `mms/mo/${mms_mo.MO_KEY}`, {
        mms_mo,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          commit(ITEM, { mms_mo });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, MO_KEY) {
      const promise = await del(dispatch, `mms/mo/${MO_KEY}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let mms_mos = state.list.filter((item) => item.MO_KEY !== MO_KEY);
          commit(LIST, { count: state.count - 1, mms_mos });
        }
        return success;
      });
      return promise;
    },
  },
};
