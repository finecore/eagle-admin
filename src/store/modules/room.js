import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import moment from "moment";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state.count;
    },
    [LIST](state) {
      return state.list;
    },
    [ITEM](state) {
      return state.item;
    },
  },
  mutations: {
    [LIST](state, { rooms }) {
      state.list = rooms;
    },
    [ITEM](state, { room }) {
      state.item = room[0] || room;
      mergeList(state.list, state.item, "id");
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page, filter = "1=1", order = "a.name", desc = "asc", limit = 100000 }) {
      if (page) {
        const { no = 1, size = getSessionStroge("rowSize") } = page;
        limit = (no - 1) * size + "," + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(dispatch, `room/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, rooms } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            rooms,
            page: _.cloneDeep(page),
          });
        } else {
          commit(LIST, []);
        }
        return { success, rooms };
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, id) {
      const promise = await item(dispatch, `room/${id}`).then(({ common: { success }, body: { room } }) => {
        if (success) {
          commit(ITEM, { room });
        }
        return { success, room: room[0] || {} };
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, room) {
      const promise = await post(dispatch, `room`, { room }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          room.id = info.insertId;
          commit(ITEM, { room });
        }
        return { success, room };
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, room) {
      const promise = await put(dispatch, `room/${room.id}`, {
        room,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          commit(ITEM, { room });
        }
        return { success, room };
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `room/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let rooms = state.list.filter((item) => item.id !== id);
          commit(LIST, { count: state.count - 1, rooms });
        }
        return success;
      });
      return promise;
    },
  },
};
