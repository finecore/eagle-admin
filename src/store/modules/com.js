import { SHOW_LOADER, HIDE_LOADER } from "@/store/mutation_types";
import _ from "lodash";

export default {
  state: {
    loader: {
      show: false,
      type: "circle",
      height: "40",
      widht: "40",
      color: "#333",
    },
  },
  getters: {
    loader(state) {
      return state.loader;
    },
  },
  mutations: {
    [SHOW_LOADER](state, loader) {
      loader.show = true;
      state.loader = _.merge(state.loader, loader);
    },
    [HIDE_LOADER](state) {
      state.loader.show = false;
    },
  },
  actions: {
    showLoader({ commit }, loader) {
      commit(SHOW_LOADER, loader);
    },
    hideLoader({ commit, state }) {
      commit(HIDE_LOADER);
    },
  },
};
