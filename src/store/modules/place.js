import { LIST, ITEM, COUNT, PAGE, SELECTED, PLACE_SEARCH_TEXT } from "@/store/mutation_types";
import { list, item, post, put, del } from "@/api";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
    [SELECTED]: 0,
    [PLACE_SEARCH_TEXT]: "",
  },
  getters: {
    [COUNT](state) {
      return state[COUNT];
    },
    [LIST](state) {
      return state[LIST];
    },
    [ITEM](state) {
      return state[ITEM];
    },
    [SELECTED](state) {
      return state[SELECTED];
    },
    [PLACE_SEARCH_TEXT](state) {
      return state[PLACE_SEARCH_TEXT];
    },
  },
  mutations: {
    [LIST](state, { count, list, page }) {
      state.count = count;
      state.list = list;
      if (page) state.page = page;
    },
    [ITEM](state, item) {
      state.item = item;
    },
    [SELECTED](state, { id, text }) {
      if (id !== undefined) state[SELECTED] = id;
      if (text !== undefined) state[PLACE_SEARCH_TEXT] = text;
    },
    [PLACE_SEARCH_TEXT](state, text) {
      state[PLACE_SEARCH_TEXT] = text;
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page, filter = "1=1", order = "reg_date", desc = "desc", limit = 10000 }) {
      if (page) {
        const { no = 1, size = getSessionStroge("rowSize") } = page;
        limit = (no - 1) * size + "," + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(dispatch, `place/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, places: list } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            list,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return list;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, id) {
      const promise = await item(dispatch, `place/${id}`).then(({ common: { success }, body: { place: item } }) => {
        if (success) {
          commit(ITEM, item);
        } else {
          commit(ITEM, []);
        }
        return item;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, place) {
      const promise = await post(dispatch, `place`, { place }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          dispatch("getList", { page: state.page });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, place) {
      const promise = await put(dispatch, `place/${place.id}`, { place }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let list = state.list.map((item) => (item.id !== place.id ? item : place));

          commit(LIST, { count: state.count, list });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `place/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          dispatch("getList", { page: state.page });
        }
        return success;
      });
      return promise;
    },
  },
};
