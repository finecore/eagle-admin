import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import moment from "moment";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state.count;
    },
    [LIST](state) {
      return state.list;
    },
    [ITEM](state) {
      return state.item;
    },
  },
  mutations: {
    [LIST](state, { count, notices, page }) {
      state.count = count;
      state.list = notices;
      if (page) state.page = page;
    },
    [ITEM](state, { notice }) {
      state.item = notice[0] || notice;
      mergeList(state.list, state.item, "id");
    },
  },
  actions: {
    async getList(
      { state, commit, dispatch },
      {
        page,
        filter = "1=1",
        between = "a.reg_date",
        begin = moment()
          .add(-3, "year")
          .format("YYYY-MM-DD"),
        end = moment()
          .add(1, "day")
          .format("YYYY-MM-DD"),
        all = 1,
      }
    ) {
      const { no = 1, size = getSessionStroge("rowSize") } = page;
      let limit = (no - 1) * size + "," + size;

      const promise = await list(dispatch, `notice/list/${filter}/${between}/${begin}/${end}/${limit}/${all}`, {
        page,
      }).then(({ common: { success }, body: { count, notices } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            notices,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return notices;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, serialno) {
      const promise = await item(dispatch, `notice/${serialno}`).then(({ common: { success }, body: { notice } }) => {
        if (success) {
          commit(ITEM, { notice });
        } else {
          commit(ITEM, []);
        }
        return notice;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, notice) {
      const promise = await post(dispatch, `notice`, { notice }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          notice.id = info.insertId;
          commit(ITEM, { notice });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, notice) {
      const promise = await put(dispatch, `notice/${notice.id}`, {
        notice,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          commit(ITEM, { notice });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `notice/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let notices = state.list.filter((item) => item.id !== id);
          commit(LIST, { count: state.count - 1, notices });
        }
        return success;
      });
      return promise;
    },
  },
};
