import { SET_ERR, SET_AUTH_ERR, SET_API_ERR, DEL_ERR } from "@/store/mutation_types";
import moment from "moment";
import _ from "lodash";

export const ERR_CODE_DEFAULT = 999;
export const ERR_CODE_AUTH = 401;
export const ERR_CODE_HOST = 400;
export const ERR_CODE_LOGOUT = 401;

export default {
  state: {
    errors: [],
    lastError: {},
    doLogout: false,
  },
  getters: {
    errorList(state) {
      return state.errors;
    },
    lastError: (state) => state.lastError,
    doLogout: (state) => state.doLogout,
  },
  mutations: {
    [SET_ERR](state, { type, code, message, detail, logout = false }) {
      detail = detail && detail instanceof Array ? detail.map((v, i) => v).join("<br/>") : detail;

      const error = {
        timestamp: new Date(),
        type,
        code,
        message,
        detail,
        logout,
      };

      state.errors.push(error);
      state.lastError = _.cloneDeep(error);
      // 인증 오류 시 로그아웃.
      if (error.code === 401) state.doLogout = true;
    },
    [DEL_ERR](state, error) {
      state.errors = _.filter(state.errors, (v) => v.timestamp !== error.timestamp);
    },
  },
  actions: {
    setErr({ state, commit }, { code = ERR_CODE_DEFAULT, message = "요청 처리 중 오류가 발생 했습니다.", detail, logout = false }) {
      commit(SET_ERR, { type: SET_ERR, code, message, detail, logout });
      return this.getters.lastError;
    },
    setLogout({ state, commit }, { code = ERR_CODE_LOGOUT, message = "로그아웃 되었습니다.", detail = "다시 로그인을 해주세요.", logout = false }) {
      commit(SET_ERR, { type: SET_AUTH_ERR, code, message, detail, logout });
      return this.getters.lastError;
    },
    setAuthErr({ state, commit }, { code = ERR_CODE_AUTH, message = "권한이 없습니다.", detail = "관리자에게 문의해 주세요.", logout = false }) {
      commit(SET_ERR, { type: SET_AUTH_ERR, code, message, detail, logout });
      return this.getters.lastError;
    },
    setApiErr({ state, commit }, { code = ERR_CODE_HOST, message = "서버 처리 중 오류가 발생 했습니다.", detail, logout = false }) {
      commit(SET_ERR, { type: SET_API_ERR, code, message, detail, logout });
      return this.getters.lastError;
    },
    delError({ state, commit }, error) {
      if (state.errors.length) commit(DEL_ERR, error);
      return this.getters.lastError;
    },
    delLoop({ state, commit }) {
      if (state.errors.length) {
        _.map(state.errors, (err) => {
          if (err.timestamp) {
            // 5 초 지난 오류 삭제.
            const time = moment(err.timestamp).format("YYYYMMDDhhmmss");
            const diff = moment().add(-5, "second").format("YYYYMMDDhhmmss");
            if (time <= diff) {
              console.log(time, diff);
              commit(DEL_ERR, err);
            }
          }
        });
      }
      return this.getters.lastError;
    },
  },
};
