import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state[COUNT];
    },
    [LIST](state) {
      return state[LIST];
    },
    [ITEM](state) {
      return state[ITEM];
    },
  },
  mutations: {
    [LIST](state, { count, isc_states, page }) {
      state.count = count;
      state.list = isc_states;
      if (page) state.page = page;
    },
    [ITEM](state, { isc_state }) {
      state.item = isc_state[0] || isc_state;

      mergeList(state.list, state.item, "serialno");
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page, filter = "1=1", order = "a.reg_date", desc = "desc", limit = 10000 }) {
      if (page) {
        const { no = 1, size = getSessionStroge("rowSize") } = page;
        limit = (no - 1) * size + "," + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(dispatch, `isc/state/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, isc_states } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            isc_states,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return isc_states;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, serialno) {
      const promise = await item(dispatch, `isc/state/${serialno}`).then(({ common: { success }, body: { isc_state } }) => {
        if (success) {
          commit(ITEM, { isc_state });
        } else {
          commit(ITEM, []);
        }
        return isc_state;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, isc_state) {
      const promise = await post(dispatch, `isc/state`, { isc_state }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let isc_states = state.list.concat(isc_state);
          commit(LIST, { count: state.count++, isc_states });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, isc_state) {
      const promise = await put(dispatch, `isc/state/${isc_state.serialno}`, {
        isc_state,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          commit(ITEM, { isc_state });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, serialno) {
      const promise = await del(dispatch, `isc/state/${serialno}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let isc_states = state.list.filter((item) => item.serialno !== serialno);
          commit(LIST, { count: state.count--, isc_states });
        }
        return success;
      });
      return promise;
    },
  },
};
