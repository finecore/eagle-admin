import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state) {
      return state.count;
    },
    [LIST](state) {
      return state.list;
    },
    [ITEM](state) {
      return state.item;
    },
  },
  mutations: {
    [LIST](state, { count, preferencess, page }) {
      state.count = count;
      state.list = preferencess;
      if (page) state.page = page;
    },
    [ITEM](state, { preferences }) {
      state.item = preferences[0] || preferences;
      mergeList(state.list, state.item, "id");
    },
  },
  actions: {
    async getList({ state, commit, dispatch }, { page = 0, filter = "1=1", order = "reg_date", desc = "desc", limit = 10000 }) {
      if (page) {
        const { no = 1, size = getSessionStroge("rowSize") } = page;
        limit = (no - 1) * size + "," + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(dispatch, `preferences/list/${filter}/${order}/${desc}/${limit}`).then(({ common: { success }, body: { count, preferencess } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            preferencess,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
        }
        return preferencess;
      });
      return promise;
    },
    async getItem({ state, commit, dispatch }, placeId) {
      const promise = await item(dispatch, `preferences/${placeId}`).then(({ common: { success }, body: { preferences } }) => {
        if (success) {
          commit(ITEM, { preferences });
        } else {
          commit(ITEM, []);
        }
        return preferences;
      });
      return promise;
    },
    async newItem({ state, commit, dispatch }, preferences) {
      const promise = await post(dispatch, `preferences`, { preferences }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          dispatch("getList", { page: state.page });
        }
        return success;
      });
      return promise;
    },
    async setItem({ state, commit, dispatch }, preferences) {
      const promise = await put(dispatch, `preferences/${preferences.id}`, {
        preferences,
      }).then(({ common: { success }, body: { info } }) => {
        if (success) {
          let preferencess = state.list.map((item) => (item.id !== preferences.id ? item : preferences));

          commit(LIST, { count: state.count, preferencess });
        }
        return success;
      });
      return promise;
    },
    async delItem({ state, commit, dispatch }, id) {
      const promise = await del(dispatch, `preferences/${id}`).then(({ common: { success }, body: { info } }) => {
        if (success) {
          dispatch("getList", { page: state.page });
        }
        return success;
      });
      return promise;
    },
  },
};
