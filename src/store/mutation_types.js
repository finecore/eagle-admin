import _ from "lodash";

// login
export const AUTH = "auth";
export const IS_AUTH = "isAuth";
export const IS_SUPERVISOR = "isSupervisor";
export const IS_ADMIN = "isAdmin";
export const IS_VIEWER = "isViewer";
export const TOKEN = "token";
export const USER_PLACES = "userPlaces";
export const DUP_ACCESS_LOGOUT = "dupAccessLogout";
export const DUP_ACCESS_FAILUE = "dupAccessFailue";

// error
export const SET_ERR = "SET_ERR";
export const SET_AUTH_ERR = "SET_AUTH_ERR";
export const SET_API_ERR = "SET_API_ERR";
export const DEL_ERR = "DEL_ERR";

// common
export const SHOW_LOADER = "SHOW_LOADER";
export const HIDE_LOADER = "HIDE_LOADER";

// data
export const LIST = "list";
export const USE_LIST = "useList";
export const ITEM = "item";
export const COUNT = "count";
export const PAGE = "page";
export const SELECTED = "selected";
export const SEL_LIST = "selList";
export const PLACE_LIST = "placeList";
export const PLACE_SEARCH_TEXT = "placeSearchText";

// merge list
export const mergeList = (oList = [], oItem, key) => {
  // console.log("-- mergeList ", { oList, oItem, key });

  if (!oList.length) {
    oList.push(oItem);
  } else {
    if (oItem[key]) {
      if (oItem.id) oItem.id = Number(oItem.id); // id 는 숫자형으로 변환.

      let inx = _.findIndex(oList, { [key]: oItem[key] }) || 0;
      let item = _.find(oList, { [key]: oItem[key] }) || {};

      item = _.merge(item, oItem);
      oList.splice(inx < 0 ? oList.length : inx, item ? 1 : 0, item); // mutation 이벤트 발생 시키려고 state 객체 자체 변경.

      // console.log("- mergeList", inx, item, key, oList);
    } else {
      console.log("-- mergeList error ", { oList, oItem, key });
    }
  }

  return oList;
};

// add list
export const addList = (list, item, order, limit = 1000) => {
  // 추가.
  if (order === "desc") {
    list.splice(0, 0, item);
    list.splice(limit);
  } else {
    list.splice(limit);
    list.splice(limit, 0, item);
  }

  // console.log("- addList", item, order, limit, list.length);
};
